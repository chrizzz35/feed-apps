package com.chrizzz35.myapp.presentation.auth

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.chrizzz35.auth.R
import com.chrizzz35.auth.presentation.AuthActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AuthActivityTest {

    @get:Rule
    val authActivityRule = ActivityTestRule<AuthActivity>(AuthActivity::class.java)

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {

    }

    @Test
    fun verifyRegisterScreenWhenBtnRegisterClicked() {
        onView(withId(R.id.btn_register)).perform(click())
    }
}
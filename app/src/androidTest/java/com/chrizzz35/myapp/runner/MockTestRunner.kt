package com.chrizzz35.myapp.runner

import android.app.Application
import android.content.Context
import android.support.test.runner.AndroidJUnitRunner
import com.chrizzz35.myapp.mock.MockMyApp

class MockTestRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, MockMyApp::class.java.name, context)
    }
}
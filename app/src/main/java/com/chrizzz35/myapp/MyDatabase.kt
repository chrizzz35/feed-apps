package com.chrizzz35.myapp

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.chrizzz35.auth.data.local.AuthDao
import com.chrizzz35.auth.domain.entity.User

@Database(entities = [ User::class ], version = 1, exportSchema = false)
abstract class MyDatabase : RoomDatabase() {
    abstract fun authDao() : AuthDao
}
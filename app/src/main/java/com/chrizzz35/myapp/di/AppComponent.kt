package com.chrizzz35.myapp.di

import android.app.Application
import com.chrizzz35.myapp.MyApp
import com.chrizzz35.myapp.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class,
    DatabaseModule::class,
    ApiModule::class,
    RepositoryModule::class,
    NavigatorModule::class,
    AndroidInjectorModule::class,
    AndroidSupportInjectionModule::class
])
interface AppComponent : AndroidInjector<MyApp> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application) : Builder
        fun build() : AppComponent
    }
}
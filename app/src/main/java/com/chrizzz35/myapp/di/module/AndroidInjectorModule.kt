package com.chrizzz35.myapp.di.module

import com.chrizzz35.auth.presentation.AuthActivity
import com.chrizzz35.auth.presentation.AuthModule
import com.chrizzz35.feed.presentation.feed.FeedActivity
import com.chrizzz35.feed.presentation.feed.FeedModule
import com.chrizzz35.feed.presentation.feed.add.FeedAddActivity
import com.chrizzz35.feed.presentation.feed.add.FeedAddModule
import com.chrizzz35.myapp.presentation.home.HomeActivity
import com.chrizzz35.myapp.presentation.home.HomeModule
import com.chrizzz35.myapp.presentation.splash.SplashActivity
import com.chrizzz35.myapp.presentation.splash.SplashModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidInjectorModule {

    @ContributesAndroidInjector(modules = [ SplashModule::class ])
    abstract fun bindSplashModule() : SplashActivity

    @ContributesAndroidInjector(modules = [ AuthModule::class ])
    abstract fun bindAuthModule() : AuthActivity

    @ContributesAndroidInjector(modules = [ HomeModule::class ])
    abstract fun bindHomeModule() : HomeActivity

    @ContributesAndroidInjector(modules = [ FeedModule::class ])
    abstract fun bindFeedModule() : FeedActivity

    @ContributesAndroidInjector(modules = [ FeedAddModule::class ])
    abstract fun bindFeedAddModule() : FeedAddActivity
}
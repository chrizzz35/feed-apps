package com.chrizzz35.myapp.di.module

import com.chrizzz35.auth.data.remote.AuthApi
import com.chrizzz35.common.data.remote.SessionApi
import com.chrizzz35.feed.data.remote.FeedApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {
    @Singleton
    @Provides
    fun provideSessionAPI(retrofit: Retrofit): SessionApi = retrofit.create(SessionApi::class.java)

    @Singleton
    @Provides
    fun provideAuthAPI(retrofit: Retrofit): AuthApi = retrofit.create(AuthApi::class.java)

    @Singleton
    @Provides
    fun provideFeedAPI(retrofit: Retrofit): FeedApi = retrofit.create(FeedApi::class.java)
}
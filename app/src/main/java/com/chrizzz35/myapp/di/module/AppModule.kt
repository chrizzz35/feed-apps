package com.chrizzz35.myapp.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: Application) : Context = application

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context) : SharedPreferences = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
}
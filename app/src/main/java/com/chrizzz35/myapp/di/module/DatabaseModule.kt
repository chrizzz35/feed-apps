package com.chrizzz35.myapp.di.module

import android.arch.persistence.room.Room
import android.content.Context
import com.chrizzz35.auth.data.local.AuthDao
import com.chrizzz35.myapp.MyDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideMyDatabase(context: Context): MyDatabase =
            Room.databaseBuilder(context, MyDatabase::class.java, MyDatabase::class.java.name).build()

    @Singleton
    @Provides
    fun provideAuthDao(myDatabase: MyDatabase) : AuthDao = myDatabase.authDao()
}
package com.chrizzz35.myapp.di.module

import android.app.Activity
import android.content.Intent
import com.chrizzz35.common.navigator.HomeNavigator
import com.chrizzz35.myapp.presentation.home.HomeActivity
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NavigatorModule {
    @Singleton
    @Provides
    fun provideHomeNavigator() : HomeNavigator = object : HomeNavigator {
        override fun redirectHome(activity: Activity) {
            activity.startActivity(Intent(activity, HomeActivity::class.java))
        }
    }
}
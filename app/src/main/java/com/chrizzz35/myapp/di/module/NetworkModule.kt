package com.chrizzz35.myapp.di.module

import android.content.SharedPreferences
import com.chrizzz35.common.config.NetworkConfig
import com.chrizzz35.common.config.NetworkConfig.Companion.TOKEN_PREFERENCES
import com.chrizzz35.common.http.HttpParser
import com.chrizzz35.myapp.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideMoshi() : Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    @Singleton
    @Provides
    fun provideMoshiConverterFactory(moshi: Moshi) : MoshiConverterFactory = MoshiConverterFactory.create(moshi)

    @Singleton
    @Provides
    fun provideRxJava2CallAdapterFactory() : RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor() : HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor, sharedPreferences: SharedPreferences) : OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
                .addInterceptor {
                    val request = it.request().newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("jwt-authorization", "Bearer ${sharedPreferences.getString(TOKEN_PREFERENCES, "")}")
                            .build()

                    it.proceed(request)
                }

        if(BuildConfig.DEBUG) {
            clientBuilder.addInterceptor(httpLoggingInterceptor)
        }

        return clientBuilder.build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, rxJava2CallAdapterFactory: RxJava2CallAdapterFactory, moshiConverterFactory: MoshiConverterFactory) : Retrofit =
            Retrofit.Builder()
                    .baseUrl(NetworkConfig.BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(moshiConverterFactory)
                    .addCallAdapterFactory(rxJava2CallAdapterFactory)
                    .build()


    @Singleton
    @Provides
    fun provideHttpParser() : HttpParser = HttpParser()
}
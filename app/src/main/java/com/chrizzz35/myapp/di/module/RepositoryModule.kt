package com.chrizzz35.myapp.di.module

import com.chrizzz35.auth.data.AuthRepositoryImpl
import com.chrizzz35.auth.data.local.AuthLocalRepositoryImpl
import com.chrizzz35.auth.data.local.AuthLocalRepository
import com.chrizzz35.auth.data.remote.AuthRemoteRepositoryImpl
import com.chrizzz35.auth.data.remote.AuthRemoteRepository
import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.common.data.SessionRepositoryImpl
import com.chrizzz35.common.data.local.SessionLocalRepositoryImpl
import com.chrizzz35.common.data.local.SessionLocalRepository
import com.chrizzz35.common.data.remote.SessionRemoteRepositoryImpl
import com.chrizzz35.common.data.remote.SessionRemoteRepository
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.feed.data.FeedRepositoryImpl
import com.chrizzz35.feed.data.local.FeedLocalRepositoryImpl
import com.chrizzz35.feed.data.local.FeedLocalRepository
import com.chrizzz35.feed.data.remote.FeedRemoteRepositoryImpl
import com.chrizzz35.feed.data.remote.FeedRemoteRepository
import com.chrizzz35.feed.domain.repository.FeedRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    //region SessionRepository
    @Singleton
    @Binds
    abstract fun bindSessionLocalRepository(sessionLocalRepositoryImpl: SessionLocalRepositoryImpl) : SessionLocalRepository

    @Singleton
    @Binds
    abstract fun bindSessionRemoteRepository(sessionRemoteRepositoryImpl: SessionRemoteRepositoryImpl) : SessionRemoteRepository

    @Singleton
    @Binds
    abstract fun bindSessionRepository(sessionRepositoryImpl: SessionRepositoryImpl) : SessionRepository
    //endregion

    //region AuthRepository
    @Singleton
    @Binds
    abstract fun bindAuthLocalRepository(authLocalRepositoryImpl: AuthLocalRepositoryImpl) : AuthLocalRepository

    @Singleton
    @Binds
    abstract fun bindAuthRemoteRepository(authRemoteRepositoryImpl: AuthRemoteRepositoryImpl) : AuthRemoteRepository

    @Singleton
    @Binds
    abstract fun bindAuthRepository(authRepositoryImpl: AuthRepositoryImpl) : AuthRepository
    //endregion

    //region FeedRepository
    @Singleton
    @Binds
    abstract fun bindFeedLocalRepository(feedLocalRepositoryImpl: FeedLocalRepositoryImpl) : FeedLocalRepository

    @Singleton
    @Binds
    abstract fun bindFeedRemoteRepository(feedRemoteRepositoryImpl: FeedRemoteRepositoryImpl) : FeedRemoteRepository

    @Singleton
    @Binds
    abstract fun bindFeedRepository(feedRepositoryImpl: FeedRepositoryImpl) : FeedRepository
    //endregion
}
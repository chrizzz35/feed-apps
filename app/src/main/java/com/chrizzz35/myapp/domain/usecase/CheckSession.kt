package com.chrizzz35.myapp.domain.usecase

import com.chrizzz35.common.domain.CompletableUseCase
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.common.http.HttpSessionExpiredException
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CheckSession @Inject constructor(
        private val sessionRepository: SessionRepository
) : CompletableUseCase<CheckSession.Param> {

    class Param

    override fun execute(param: Param): Completable {
        with(param) {
            return sessionRepository.getSessionToken()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMapCompletable {
                        if(it == "") throw HttpSessionExpiredException("")
                        else Completable.complete()
                    }
        }
    }
}
package com.chrizzz35.myapp.mock

import android.app.Application
import com.chrizzz35.myapp.di.AppComponent
import com.chrizzz35.myapp.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class,
    DatabaseModule::class,
    ApiModule::class,
    MockRepositoryModule::class,
    NavigatorModule::class,
    AndroidInjectorModule::class,
    AndroidSupportInjectionModule::class
])
interface MockComponent : AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application) : Builder
        fun build() : MockComponent
    }
}
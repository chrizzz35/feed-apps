package com.chrizzz35.myapp.mock

import com.chrizzz35.myapp.MyApp
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class MockMyApp : MyApp() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerMockComponent.builder().application(this).build()
}
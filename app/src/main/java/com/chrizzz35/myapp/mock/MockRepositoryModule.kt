package com.chrizzz35.myapp.mock

import com.chrizzz35.auth.data.MockAuthRepository
import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.common.data.MockSessionRepository
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.feed.data.MockFeedRepository
import com.chrizzz35.feed.domain.repository.FeedRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class MockRepositoryModule {

    @Singleton
    @Binds
    abstract fun bindSessionRepository(mockSessionRepository: MockSessionRepository) : SessionRepository

    @Singleton
    @Binds
    abstract fun bindAuthRepository(mockAuthRepository: MockAuthRepository) : AuthRepository

    @Singleton
    @Binds
    abstract fun bindFeedRepository(mockFeedRepository: MockFeedRepository) : FeedRepository
}
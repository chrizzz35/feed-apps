package com.chrizzz35.myapp.presentation.home

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.chrizzz35.auth.presentation.profile.info.ProfileInfoFragment
import com.chrizzz35.common.dialog.optionmenu.OptionMenuDialogFragment
import com.chrizzz35.feed.presentation.feed.add.FeedAddActivity
import com.chrizzz35.feed.presentation.feed.list.FeedListFragment
import com.chrizzz35.myapp.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : DaggerAppCompatActivity(), FeedListFragment.FeedListFragmentListener,
        ProfileInfoFragment.ProfileInfoFragmentListener, OptionMenuDialogFragment.OptionMenuDialogFragmentListener {

    private val menuList = arrayListOf("About Us", "Logout")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setupActionBar()
        setupBottomNavigation()

        if(savedInstanceState == null) {
            val feedListFragment = FeedListFragment.newInstance()
            supportFragmentManager.beginTransaction()
                    .add(R.id.fl_content, feedListFragment, null)
                    .commitNow()
        }
    }

    private fun setupActionBar() {
        supportActionBar?.setLogo(R.mipmap.ic_launcher)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun setupBottomNavigation() {
        bnv_menu.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.menu_home -> {
                    val feedListFragment = FeedListFragment.newInstance()
                    supportFragmentManager.beginTransaction()
                            .replace(R.id.fl_content, feedListFragment, null)
                            .commitNow()

                    true
                }
                R.id.menu_add -> {
//                    startActivity(Intent(this, FeedAddActivity::class.java))
                    Toast.makeText(this, "Add Feed", Toast.LENGTH_LONG).show()

                    false
                }
                R.id.menu_profile -> {
//                    val profileInfoFragment = ProfileInfoFragment.newInstance()
//                    supportFragmentManager.beginTransaction()
//                            .replace(R.id.fl_content, profileInfoFragment, null)
//                            .commitNow()
                    Toast.makeText(this, "Profile", Toast.LENGTH_LONG).show()
                    false
                }
                else -> false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_option_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.menu_setting -> {
                val optionMenuDialogFragment = OptionMenuDialogFragment.newInstance(menuList)
                optionMenuDialogFragment.show(supportFragmentManager, null)

                return true
            }
        }

        return true
    }

    override fun onSelectedMenu(position: Int) {
        when(position) {
            0 -> { // About Us
                // TODO
                Toast.makeText(this, "About Us", Toast.LENGTH_LONG).show()
            }
            1 -> { // Logout
                // TODO
                Toast.makeText(this, "Logout", Toast.LENGTH_LONG).show()
            }
        }
    }
}

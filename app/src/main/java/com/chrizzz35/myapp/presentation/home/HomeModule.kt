package com.chrizzz35.myapp.presentation.home

import com.chrizzz35.auth.presentation.profile.info.ProfileInfoFragment
import com.chrizzz35.auth.presentation.profile.info.ProfileInfoModule
import com.chrizzz35.feed.presentation.feed.list.FeedListFragment
import com.chrizzz35.feed.presentation.feed.list.FeedListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeModule {

    @ContributesAndroidInjector(modules = [ FeedListModule::class ])
    abstract fun bindFeedListModule() : FeedListFragment

    @ContributesAndroidInjector(modules = [ ProfileInfoModule::class ])
    abstract fun bindProfileModule() : ProfileInfoFragment
}
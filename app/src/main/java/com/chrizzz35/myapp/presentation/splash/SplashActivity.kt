package com.chrizzz35.myapp.presentation.splash

import android.animation.Animator
import android.arch.lifecycle.Observer
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import com.chrizzz35.auth.presentation.AuthActivity
import com.chrizzz35.myapp.R
import com.chrizzz35.myapp.databinding.ActivitySplashBinding
import com.chrizzz35.myapp.presentation.home.HomeActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_splash.*
import javax.inject.Inject

class SplashActivity : DaggerAppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding

    @Inject
    lateinit var splashPresenter: SplashPresenter

    @Inject
    lateinit var splashViewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        binding.viewModel = splashViewModel
        binding.presenter = splashPresenter

        observeCheckSessionAvailability()
        splashPresenter.checkSessionAvailability()
    }

    private fun observeCheckSessionAvailability() {
        splashViewModel.isSessionAvailable.observe(this,  Observer {
            animateSplash(it ?: false)
        })
    }

    private fun animateSplash(isSessionAvailable: Boolean) {
        iv_splash_icon.animate()
                .alpha(1.0f)
                .setDuration(1000)
                .setListener(object: Animator.AnimatorListener {
                    override fun onAnimationRepeat(p0: Animator?) {

                    }

                    override fun onAnimationEnd(p0: Animator?) {
                        if(isSessionAvailable) redirectHome()
                        else redirectAuth()
                    }

                    override fun onAnimationCancel(p0: Animator?) {

                    }

                    override fun onAnimationStart(p0: Animator?) {

                    }
                })
    }

    private fun redirectAuth() {
        startActivity(Intent(this, AuthActivity::class.java))
        finish()
    }

    private fun redirectHome() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }
}

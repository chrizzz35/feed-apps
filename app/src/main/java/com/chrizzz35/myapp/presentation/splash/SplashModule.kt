package com.chrizzz35.myapp.presentation.splash

import android.arch.lifecycle.ViewModelProviders
import com.chrizzz35.myapp.domain.usecase.CheckSession
import dagger.Module
import dagger.Provides

@Module
class SplashModule {
    @Provides
    fun provideSplashViewModel(splashActivity: SplashActivity) : SplashViewModel =
            ViewModelProviders.of(splashActivity).get(SplashViewModel::class.java)

    @Provides
    fun provideSplashPresenter(splashViewModel: SplashViewModel, checkSession: CheckSession): SplashPresenter =
            SplashPresenter(splashViewModel, checkSession)
}
package com.chrizzz35.myapp.presentation.splash

import com.chrizzz35.myapp.domain.usecase.CheckSession

class SplashPresenter (
        private val splashViewModel: SplashViewModel,
        private val checkSession: CheckSession
) {
    fun checkSessionAvailability() {
        splashViewModel.compositeDisposable.add(checkSession.execute(CheckSession.Param())
                .subscribe({
                    splashViewModel.isSessionAvailable.value = true
                }, {
                    splashViewModel.isSessionAvailable.value = false
                }))
    }
}
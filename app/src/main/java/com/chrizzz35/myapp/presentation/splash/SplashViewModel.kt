package com.chrizzz35.myapp.presentation.splash

import android.arch.lifecycle.MutableLiveData
import com.chrizzz35.common.base.BaseViewModel

class SplashViewModel : BaseViewModel() {

    val isSessionAvailable = MutableLiveData<Boolean>()
}
package com.chrizzz35.auth.data

import com.chrizzz35.auth.data.local.AuthLocalRepository
import com.chrizzz35.auth.data.remote.AuthRemoteRepository
import com.chrizzz35.auth.domain.entity.User
import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.common.domain.repository.SessionRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
        private val authLocalRepository: AuthLocalRepository,
        private val authRemoteRepository: AuthRemoteRepository,
        private val sessionRepository: SessionRepository
) : AuthRepository {
    
    override fun login(username: String, password: String): Completable {
        return authRemoteRepository.login(username, password)
                .flatMapCompletable {
                    with(it) {
                        return@flatMapCompletable sessionRepository.saveSessionToken(token ?: "")
                                .andThen(saveProfile(User(id, name, phone, email, gender, dob, photo, cover)))
                    }
                }
    }

    override fun logout(): Completable {
        return authRemoteRepository.logout()
    }

    override fun register(name: String, phone: String, email: String, gender: Int, dob: String, password: String): Completable {
        return authRemoteRepository.register(name, phone, email, gender, dob, password)
    }

    override fun getProfile(): Single<User> {
        return authLocalRepository.getProfile()
    }

    override fun saveProfile(user: User): Completable {
        return authLocalRepository.saveProfile(user)
    }
}
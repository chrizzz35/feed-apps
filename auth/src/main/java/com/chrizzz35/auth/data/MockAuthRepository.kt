package com.chrizzz35.auth.data

import com.chrizzz35.auth.domain.entity.User
import com.chrizzz35.auth.domain.repository.AuthRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class MockAuthRepository @Inject constructor() : AuthRepository {
    override fun login(username: String, password: String): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logout(): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun register(name: String, phone: String, email: String, gender: Int, dob: String, password: String): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getProfile(): Single<User> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveProfile(user: User): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
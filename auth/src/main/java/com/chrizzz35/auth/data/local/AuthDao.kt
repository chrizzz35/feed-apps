package com.chrizzz35.auth.data.local

import android.arch.persistence.room.*
import com.chrizzz35.auth.domain.entity.User
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface AuthDao {
    @Query("select * from User")
    fun getAllUser() : Single<List<User>>

    @Insert
    fun insertUser(user: User)

    @Update
    fun updateUser(user: User)

    @Delete
    fun deleteUser(user: User)
}
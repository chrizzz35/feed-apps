package com.chrizzz35.auth.data.local

import com.chrizzz35.auth.domain.entity.User
import io.reactivex.Completable
import io.reactivex.Single

interface AuthLocalRepository {
    fun getProfile() : Single<User>
    fun saveProfile(user: User) : Completable
}
package com.chrizzz35.auth.data.local

import com.chrizzz35.auth.domain.entity.User
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthLocalRepositoryImpl @Inject constructor(
        private val authDao: AuthDao
): AuthLocalRepository {
    override fun getProfile(): Single<User> {
        return authDao.getAllUser()
                .map { it[0] }
    }

    override fun saveProfile(user: User): Completable {
        return Completable.create {
            try {
                authDao.deleteUser(user)
                authDao.insertUser(user)

                it.onComplete()
            }
            catch (e: Exception) {
                it.onError(e)
            }
        }
    }
}
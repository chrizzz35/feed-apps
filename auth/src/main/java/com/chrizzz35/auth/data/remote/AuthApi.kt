package com.chrizzz35.auth.data.remote

import com.chrizzz35.auth.data.remote.request.LoginRequest
import com.chrizzz35.auth.data.remote.request.RegisterRequest
import com.chrizzz35.auth.data.remote.response.LoginResponse
import com.chrizzz35.auth.data.remote.response.RegisterResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {
    @POST("/api/auth/login")
    fun login(@Body loginRequest: LoginRequest) : Single<LoginResponse>

    @POST("/api/auth/logout")
    fun logout() : Completable

    @POST("/api/auth/register")
    fun register(@Body registerRequest: RegisterRequest) : Single<RegisterResponse>
}
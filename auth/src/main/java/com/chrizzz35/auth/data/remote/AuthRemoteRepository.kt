package com.chrizzz35.auth.data.remote

import com.chrizzz35.auth.data.remote.response.LoginResponseData
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRemoteRepository {
    fun login(username: String, password: String) : Single<LoginResponseData>
    fun logout() : Completable
    fun register(name: String, phone: String, email: String, gender: Int, dob: String, password: String) : Completable
}
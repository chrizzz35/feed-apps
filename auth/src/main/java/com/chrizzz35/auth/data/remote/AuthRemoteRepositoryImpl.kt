package com.chrizzz35.auth.data.remote

import com.chrizzz35.auth.data.remote.request.LoginRequest
import com.chrizzz35.auth.data.remote.request.RegisterRequest
import com.chrizzz35.auth.data.remote.response.LoginResponseData
import com.chrizzz35.common.http.HttpParser
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRemoteRepositoryImpl @Inject constructor(
        private val authApi: AuthApi,
        private val httpParser: HttpParser
): AuthRemoteRepository {

    override fun login(username: String, password: String): Single<LoginResponseData> {
        return authApi.login(LoginRequest(username, password)).map { httpParser.parseResponse(it) }
    }

    override fun logout(): Completable {
        return authApi.logout()
    }

    override fun register(name: String, phone: String, email: String, gender: Int, dob: String, password: String): Completable {
        return authApi.register(RegisterRequest(name, phone, email, gender, dob, password))
                .flatMapCompletable { httpParser.parseDefaultResponse(it) }
    }
}
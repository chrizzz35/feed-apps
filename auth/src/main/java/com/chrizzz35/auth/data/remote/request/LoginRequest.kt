package com.chrizzz35.auth.data.remote.request

import com.squareup.moshi.Json

data class LoginRequest (
        @Json(name="username") val username: String,
        @Json(name="password") val password: String
)
package com.chrizzz35.auth.data.remote.request

import com.squareup.moshi.Json

data class RegisterRequest(
    @Json(name = "name") val name: String,
    @Json(name = "phone") val phone: String,
    @Json(name = "email") val email: String,
    @Json(name = "gender") val gender: Int,
    @Json(name = "birth_date") val dob: String,
    @Json(name = "password") val password: String
)
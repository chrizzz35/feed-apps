package com.chrizzz35.auth.data.remote.response

import com.chrizzz35.common.http.HttpResponse
import com.squareup.moshi.Json

data class RegisterResponse (
        @Json(name = "status_code") override val statusCode: Int?,
        @Json(name = "message") override val message: String?,
        @Json(name = "data") override val data: RegisterResponseData?
): HttpResponse<RegisterResponseData>()

data class RegisterResponseData (
        @Json(name = "id") val id: Int?,
        @Json(name = "name") val name: String?,
        @Json(name = "phone") val phone: String?,
        @Json(name = "email") val email: String?,
        @Json(name = "gender") val gender: Int?,
        @Json(name = "birth_date") val dob: String?
)
package com.chrizzz35.auth.domain.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "User")
@Parcelize
data class User (
        @Json(name = "id")
        @ColumnInfo(name = "id")
        @PrimaryKey
        val id: Int?,

        @Json(name = "name")
        @ColumnInfo(name = "name")
        val name: String?,

        @Json(name = "phone")
        @ColumnInfo(name = "phone")
        val phone: String?,

        @Json(name = "email")
        @ColumnInfo(name = "email")
        val email: String?,

        @Json(name = "gender")
        @ColumnInfo(name = "gender")
        val gender: Int?,

        @Json(name = "birth_date")
        @ColumnInfo(name = "birth_date")
        val dob: String?,

        @Json(name = "photo")
        @ColumnInfo(name = "photo")
        val photo: String?,

        @Json(name = "cover")
        @ColumnInfo(name = "cover")
        val cover: String?
) : Parcelable
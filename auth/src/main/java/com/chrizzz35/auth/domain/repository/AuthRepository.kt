package com.chrizzz35.auth.domain.repository

import com.chrizzz35.auth.domain.entity.User
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRepository {
    fun login(username: String, password: String) : Completable
    fun logout() : Completable
    fun register(name: String, phone: String, email: String, gender: Int, dob: String, password: String) : Completable

    fun getProfile() : Single<User>
    fun saveProfile(user: User) : Completable
}
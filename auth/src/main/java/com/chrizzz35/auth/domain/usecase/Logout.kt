package com.chrizzz35.auth.domain.usecase

import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.common.domain.CompletableUseCase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class Logout @Inject constructor(
        private val authRepository: AuthRepository
): CompletableUseCase<Logout.Param> {
    class Param

    override fun execute(param: Param): Completable {
        with(param) {
            return authRepository.logout()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }
}
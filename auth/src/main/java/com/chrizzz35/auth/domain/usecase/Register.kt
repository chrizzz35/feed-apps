package com.chrizzz35.auth.domain.usecase

import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.common.domain.CompletableUseCase
import com.chrizzz35.common.extension.toMD5
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class Register @Inject constructor(
        private val authRepository: AuthRepository
): CompletableUseCase<Register.Param> {

    class Param(val name: String, val phone: String, val email: String, val gender: Int, val dob: String, val password: String)

    override fun execute(param: Param): Completable {
        with(param) {
            return authRepository.register(name, phone, email, gender, dob, password.toMD5())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }
}
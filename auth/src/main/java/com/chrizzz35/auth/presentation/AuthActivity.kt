package com.chrizzz35.auth.presentation

import android.os.Bundle
import com.chrizzz35.auth.R
import com.chrizzz35.auth.presentation.auth.login.LoginFragment
import com.chrizzz35.auth.presentation.auth.register.RegisterFragment
import com.chrizzz35.common.navigator.HomeNavigator
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class AuthActivity : DaggerAppCompatActivity(), LoginFragment.LoginFragmentListener, RegisterFragment.RegisterFragmentListener {

    @Inject
    lateinit var homeNavigator: HomeNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        if(savedInstanceState == null) {
            val loginFragment = LoginFragment.newInstance()
            supportFragmentManager.beginTransaction()
                    .add(R.id.fl_content, loginFragment, null)
                    .commitNow()
        }
    }

    override fun redirectRegister() {
        val registerFragment = RegisterFragment.newInstance()
        supportFragmentManager.beginTransaction()
                .add(R.id.fl_content, registerFragment, null)
                .addToBackStack("")
                .commit()
    }

    override fun redirectHome() {
        homeNavigator.redirectHome(this)
        finish()
    }
}

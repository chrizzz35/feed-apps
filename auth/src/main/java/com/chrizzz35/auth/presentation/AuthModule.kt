package com.chrizzz35.auth.presentation

import com.chrizzz35.auth.presentation.auth.login.LoginFragment
import com.chrizzz35.auth.presentation.auth.login.LoginModule
import com.chrizzz35.auth.presentation.auth.register.RegisterFragment
import com.chrizzz35.auth.presentation.auth.register.RegisterModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AuthModule {

    @ContributesAndroidInjector(modules = [ LoginModule::class ])
    abstract fun bindLoginModule() : LoginFragment

    @ContributesAndroidInjector(modules = [ RegisterModule::class ])
    abstract fun bindRegisterModule() : RegisterFragment
}
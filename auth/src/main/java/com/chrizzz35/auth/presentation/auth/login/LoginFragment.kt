package com.chrizzz35.auth.presentation.auth.login

import android.arch.lifecycle.Observer
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.chrizzz35.auth.R
import com.chrizzz35.auth.databinding.FragmentLoginBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class LoginFragment : DaggerFragment() {

    private var listener: LoginFragmentListener? = null
    private lateinit var binding: FragmentLoginBinding

    @Inject
    lateinit var loginPresenter: LoginPresenter
    @Inject
    lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)

        binding.setLifecycleOwner(this)
        binding.viewModel = loginViewModel
        binding.presenter = loginPresenter
        binding.listener = listener

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observeLoginSuccess()
        observeLoginFailed()
    }

    private fun observeLoginSuccess() {
        loginViewModel.loginSuccess.observe(this, Observer {
            if(it == true) {
                listener?.redirectHome()
            }
        })
    }

    private fun observeLoginFailed() {
        loginViewModel.loginFailed.observe(this, Observer {
            val message: String? = when(it) {
                is LoginMessageType.UsernameEmpty -> getString(R.string.login_message_username_empty)
                is LoginMessageType.PasswordEmpty -> getString(R.string.login_message_password_empty)
                is LoginMessageType.ApiFailed -> it.message
                is LoginMessageType.NetworkFailed -> getString(R.string.network_failed_message)
                else -> null
            }

            message?.run {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is LoginFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement LoginFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface LoginFragmentListener {
        fun redirectHome()
        fun redirectRegister()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                LoginFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}

package com.chrizzz35.auth.presentation.auth.login

import android.arch.lifecycle.ViewModelProviders
import com.chrizzz35.auth.domain.usecase.Login
import dagger.Module
import dagger.Provides

@Module
class LoginModule {
    @Provides
    fun provideLoginViewModel(loginFragment: LoginFragment): LoginViewModel =
            ViewModelProviders.of(loginFragment).get(LoginViewModel::class.java)

    @Provides
    fun provideLoginPresenter(loginViewModel: LoginViewModel, login: Login): LoginPresenter =
            LoginPresenter(loginViewModel, login)
}
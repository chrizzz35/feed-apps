package com.chrizzz35.auth.presentation.auth.login

import com.chrizzz35.auth.domain.usecase.Login
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

class LoginPresenter (
        private val loginViewModel: LoginViewModel,
        private val login: Login
) {
    private fun isValidInput() : Boolean {
        with(loginViewModel) {
            val username = username.value ?: ""
            val password = password.value ?: ""

            if(username == "") {
                loginFailed.value = LoginMessageType.UsernameEmpty()

                return false
            }

            if(password == "") {
                loginFailed.value = LoginMessageType.PasswordEmpty()

                return false
            }

            return true
        }
    }

    fun login() {
        with(loginViewModel) {
            if(loginLoading.value != true) {
                if(isValidInput()) {
                    compositeDisposable.add(login.execute(Login.Param(username.value!!, password.value!!))
                            .doOnSubscribe {
                                loginLoading.value = true
                                loginSuccess.value = null
                                loginFailed.value = null
                            }
                            .doFinally {
                                loginLoading.value = false
                            }
                            .subscribe({
                                loginSuccess.value = true
                                loginFailed.value = null
                            }, {
                                loginSuccess.value = null
                                loginFailed.value = when(it) {
                                    is SocketTimeoutException -> LoginMessageType.NetworkFailed()
                                    is TimeoutException -> LoginMessageType.NetworkFailed()
                                    else -> LoginMessageType.ApiFailed(it.message)
                                }
                            })
                    )
                }
            }
        }
    }
}
package com.chrizzz35.auth.presentation.auth.login

import android.arch.lifecycle.MutableLiveData
import com.chrizzz35.common.base.BaseViewModel
import com.chrizzz35.common.custom.SingleLiveEvent

class LoginViewModel : BaseViewModel() {

    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    val loginLoading = MutableLiveData<Boolean>()

    val loginSuccess = SingleLiveEvent<Boolean>()
    val loginFailed = SingleLiveEvent<LoginMessageType>()
}

sealed class LoginMessageType {
    class UsernameEmpty : LoginMessageType()
    class PasswordEmpty : LoginMessageType()
    class ApiFailed(val message: String?) : LoginMessageType()
    class NetworkFailed : LoginMessageType()
}
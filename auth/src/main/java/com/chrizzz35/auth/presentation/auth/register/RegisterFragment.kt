package com.chrizzz35.auth.presentation.auth.register

import android.arch.lifecycle.Observer
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.chrizzz35.auth.R
import com.chrizzz35.auth.databinding.FragmentRegisterBinding
import com.chrizzz35.common.dialog.dobpicker.DobPickerDialogFragment
import com.chrizzz35.common.dialog.genderpicker.GenderPickerDialogFragment
import com.chrizzz35.common.extension.dateFormat
import com.chrizzz35.common.extension.hideKeyboard
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class RegisterFragment : DaggerFragment() {

    private var listener: RegisterFragmentListener? = null
    private lateinit var binding: FragmentRegisterBinding

    @Inject
    lateinit var registerViewModel: RegisterViewModel
    @Inject
    lateinit var registerPresenter: RegisterPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)

        binding.setLifecycleOwner(this)
        binding.fragment = this
        binding.viewModel = registerViewModel
        binding.presenter = registerPresenter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observeRegisterSuccess()
        observeRegisterFailed()
    }

    private fun observeRegisterSuccess() {
        registerViewModel.registerSuccess.observe(this, Observer {
            if(it == true) activity?.onBackPressed()
        })
    }

    private fun observeRegisterFailed() {
        registerViewModel.registerFailed.observe(this, Observer {
            it?.run {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
        })
    }

    fun openGenderDialog() {
        if(registerViewModel.isRegisterLoading.value != true) {
            activity?.hideKeyboard()

            val pickerDialogFragment = GenderPickerDialogFragment.newInstance()
            pickerDialogFragment.setPickerDialogFragmentListener(object : GenderPickerDialogFragment.GenderPickerDialogFragmentListener {
                override fun onGenderSelected(selectedGender: Int) {
                    registerViewModel.gender.value = selectedGender
                }
            })
            pickerDialogFragment.show(childFragmentManager, null)
        }
    }

    fun openDobDialog() {
        if(registerViewModel.isRegisterLoading.value != true) {
            activity?.hideKeyboard()

            val dobPickerDialogFragment = DobPickerDialogFragment.newInstance()
            dobPickerDialogFragment.setDobPickerDialogFragmentListener(object : DobPickerDialogFragment.DobPickerDialogFragmentListener {
                override fun onDobSelected(selectedDob: String) {
                    registerViewModel.dob.value = selectedDob.dateFormat("yyyy-MM-dd", "dd-MM-yyyy")
                }
            })
            dobPickerDialogFragment.show(childFragmentManager, null)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is RegisterFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement RegisterFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface RegisterFragmentListener

    companion object {
        @JvmStatic
        fun newInstance() =
                RegisterFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}

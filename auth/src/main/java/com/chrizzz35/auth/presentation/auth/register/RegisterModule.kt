package com.chrizzz35.auth.presentation.auth.register

import android.arch.lifecycle.ViewModelProviders
import com.chrizzz35.auth.domain.usecase.Register
import dagger.Module
import dagger.Provides

@Module
class RegisterModule {
    @Provides
    fun provideRegisterViewModel(registerFragment: RegisterFragment): RegisterViewModel =
            ViewModelProviders.of(registerFragment).get(RegisterViewModel::class.java)

    @Provides
    fun provideRegisterPresenter(registerViewModel: RegisterViewModel, register: Register): RegisterPresenter =
            RegisterPresenter(registerViewModel, register)
}
package com.chrizzz35.auth.presentation.auth.register

import com.chrizzz35.auth.domain.usecase.Register
import com.chrizzz35.common.extension.isValidEmail
import com.chrizzz35.common.extension.isValidPhone

class RegisterPresenter (
        private val registerViewModel: RegisterViewModel,
        private val register: Register
) {

    private fun isValidInput(): Boolean {
        with(registerViewModel) {
            var result = true

            nameInvalidMessage.value = ""
            phoneInvalidMessage.value = ""
            emailInvalidMessage.value = ""
            genderInvalidMessage.value = ""
            dobInvalidMessage.value = ""
            passwordInvalidMessage.value = ""
            confirmPasswordInvalidMessage.value = ""

            val name = name.value ?: ""
            val phone = phone.value ?: ""
            val email = email.value ?: ""
            val gender = gender.value ?: -1
            val dob = dob.value ?: ""
            val password = password.value ?: ""
            val confirmPassword = confirmPassword.value ?: ""

            if(name == "") {
                nameInvalidMessage.value = "Please fill name"
                result = false
            }

            if(phone == "") {
                phoneInvalidMessage.value = "Please fill phone"
                result = false
            }
            else if(!phone.isValidPhone()) {
                phoneInvalidMessage.value = "Invalid phone format"
                result = false
            }

            if(email == "") {
                emailInvalidMessage.value = "Please fill email"
                result = false
            }
            else if(!email.isValidEmail()) {
                emailInvalidMessage.value = "Invalid email format"
                result = false
            }

            if(gender == -1) {
                genderInvalidMessage.value = "Please fill gender"
                result = false
            }

            if(dob == "") {
                dobInvalidMessage.value = "Please fill dob"
                result = false
            }

            if(password == "") {
                passwordInvalidMessage.value = "Please fill password"
                result = false
            }

            if(confirmPassword == "") {
                confirmPasswordInvalidMessage.value = "Please fill confirm password"
                result = false
            }
            else if(confirmPassword != password) {
                confirmPasswordInvalidMessage.value = "Confirm password is different"
                result = false
            }

            return result
        }
    }

    fun register() {
        with(registerViewModel) {
            if(registerViewModel.isRegisterLoading.value != true) {
                if (isValidInput()) {
                    compositeDisposable.add(register.execute(Register.Param(name.value!!, phone.value!!, email.value!!, gender.value!!, dob.value!!, password.value!!))
                            .doOnSubscribe {
                                isRegisterLoading.value = true
                                registerSuccess.value = null
                                registerFailed.value = null
                            }
                            .doFinally {
                                isRegisterLoading.value = false
                            }
                            .subscribe({
                                registerSuccess.value = true
                                registerFailed.value = null
                            }, {
                                registerSuccess.value = null
                                registerFailed.value = it.message
                            }))
                }
            }
        }
    }
}
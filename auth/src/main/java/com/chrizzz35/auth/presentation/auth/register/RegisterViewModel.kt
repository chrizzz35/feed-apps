package com.chrizzz35.auth.presentation.auth.register

import android.arch.lifecycle.MutableLiveData
import com.chrizzz35.common.base.BaseViewModel
import com.chrizzz35.common.custom.SingleLiveEvent

class RegisterViewModel : BaseViewModel() {

    val name = MutableLiveData<String>()
    val phone = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val gender = MutableLiveData<Int>()
    val dob = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()

    val isRegisterLoading = MutableLiveData<Boolean>()

    val nameInvalidMessage = MutableLiveData<String>()
    val phoneInvalidMessage = MutableLiveData<String>()
    val emailInvalidMessage = MutableLiveData<String>()
    val genderInvalidMessage = MutableLiveData<String>()
    val dobInvalidMessage = MutableLiveData<String>()
    val passwordInvalidMessage = MutableLiveData<String>()
    val confirmPasswordInvalidMessage = MutableLiveData<String>()

    val registerSuccess = SingleLiveEvent<Boolean>()
    val registerFailed = SingleLiveEvent<String>()
}
package com.chrizzz35.auth.presentation.profile.info

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.chrizzz35.auth.R
import com.chrizzz35.common.dialog.imagepicker.StringPickerDialogFragment
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_profile_info.*
import java.io.File
import javax.inject.Inject

const val TYPE_PROFILE_PICTURE = "Profile Picture"
const val TYPE_COVER_PICTURE = "Cover Picture"

class ProfileInfoFragment : DaggerFragment() {

    private var listener: ProfileInfoFragmentListener? = null

    private var selectedPictureType: String? = null
    private val pictureType = arrayOf(
            TYPE_PROFILE_PICTURE,
            TYPE_COVER_PICTURE
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_profile.setOnClickListener { updateImage() }
    }

    private fun updateImage() {
        AlertDialog.Builder(context)
                .setItems(pictureType) { dialog, position ->
                    selectedPictureType = when(position) {
                        0 -> TYPE_PROFILE_PICTURE
                        else -> TYPE_COVER_PICTURE
                    }

                    dialog.dismiss()
                    showImageTypeDialog()
                }
                .create()
                .show()
    }

    private fun showImageTypeDialog() {
        val stringPickerDialogFragment = StringPickerDialogFragment.newInstance()
        stringPickerDialogFragment.setStringPickerDialogFragmentListener(object : StringPickerDialogFragment.StringPickerDialogFragmentListener {
            override fun onImagePicked(file: File) {
                val imageView = when(selectedPictureType) {
                    TYPE_PROFILE_PICTURE -> iv_profile
                    else -> iv_cover
                }

                Glide.with(imageView.context)
                        .setDefaultRequestOptions(
                                RequestOptions()
                                        .skipMemoryCache(true)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                        .load(file)
                        .into(imageView)
            }
        })
        stringPickerDialogFragment.show(childFragmentManager, null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProfileInfoFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement ProfileInfoFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface ProfileInfoFragmentListener
    
    companion object {
        @JvmStatic
        fun newInstance() =
                ProfileInfoFragment().apply {
                    arguments = Bundle().apply {
                        
                    }
                }
    }
}

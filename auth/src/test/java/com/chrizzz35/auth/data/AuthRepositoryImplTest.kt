package com.chrizzz35.auth.data

import com.chrizzz35.auth.data.local.AuthLocalRepository
import com.chrizzz35.auth.data.remote.AuthRemoteRepository
import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.auth.util.AuthTestUtil
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.common.http.HttpFailedException
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class AuthRepositoryImplTest {

    @Mock
    lateinit var authLocalRepository: AuthLocalRepository
    @Mock
    lateinit var authRemoteRepository: AuthRemoteRepository
    @Mock
    lateinit var sessionRepository: SessionRepository
    lateinit var authRepository: AuthRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        authRepository = AuthRepositoryImpl(authLocalRepository, authRemoteRepository, sessionRepository)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Login, return success, then save session`() {
        with(AuthTestUtil) {
            // Given
            Mockito.`when`(authRemoteRepository.login(username, password))
                    .thenReturn(Single.just(loginResponseSuccess.data))
            Mockito.`when`(sessionRepository.saveSessionToken(loginResponseSuccess.data?.token ?: ""))
                    .thenReturn(Completable.complete())
            Mockito.`when`(authLocalRepository.saveProfile(user))
                    .thenReturn(Completable.complete())

            // When
            val testObserver = authRepository.login(username, password).test()

            // Then
            // testObserver.awaitTerminalEvent()
            testObserver.assertComplete()

            Mockito.verify(authRemoteRepository).login(username, password)
            Mockito.verify(sessionRepository).saveSessionToken(loginResponseSuccess.data?.token ?: "")
            Mockito.verify(authLocalRepository).saveProfile((user))
        }
    }

    @Test
    fun `Login, return failed, no session saved`() {
        with(AuthTestUtil) {
            // Given
            Mockito.`when`(authRemoteRepository.login(username, password))
                    .thenReturn(Single.error(HttpFailedException(loginResponseFailed.message)))

            // When
            val testObserver = authRepository.login(username, password).test()

            // Then
            // testObserver.awaitTerminalEvent()
            testObserver.assertError {
                it is HttpFailedException && it.message == loginResponseFailed.message
            }

            Mockito.verify(authRemoteRepository).login(username, password)
            Mockito.verify(sessionRepository, Mockito.never()).saveSessionToken(loginResponseFailed.data?.token ?: "")
            Mockito.verify(authLocalRepository, Mockito.never()).saveProfile(user)
        }
    }
}
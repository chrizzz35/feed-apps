package com.chrizzz35.auth.data.remote.request

import com.chrizzz35.auth.util.AuthTestUtil
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.junit.After
import org.junit.Before
import org.junit.Test

class RegisterRequestTest {

    val moshiAdapter = Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RegisterRequest::class.java)

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {

    }

    @Test
    fun `RegisterRequest, from JSON to Object`() {
        with(AuthTestUtil) {
            val request = moshiAdapter.fromJson(registerRequestJson)

            assert(registerRequest == request)
        }
    }
}
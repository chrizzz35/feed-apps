package com.chrizzz35.auth.data.remote.response

import com.chrizzz35.auth.util.AuthTestUtil
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.junit.After
import org.junit.Before
import org.junit.Test

class LoginResponseTest {

    val moshiAdapter = Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(LoginResponse::class.java)

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {

    }

    @Test
    fun `LoginResponse Success, from JSON to Object`() {
        with(AuthTestUtil) {
            val responseSuccess = moshiAdapter.fromJson(loginResponseSuccessJson)

            assert(loginResponseSuccess == responseSuccess)
        }
    }

    @Test
    fun `LoginResponse Failed, from JSON to Object`() {
        with(AuthTestUtil) {
            val responseFailed = moshiAdapter.fromJson(loginResponseFailedJson)

            assert(loginResponseFailed == responseFailed)
        }
    }
}
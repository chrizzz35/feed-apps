package com.chrizzz35.auth.data.remote.response

import com.chrizzz35.auth.util.AuthTestUtil
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.junit.After
import org.junit.Before
import org.junit.Test

class RegisterResponseTest {

    val moshiAdapter = Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RegisterResponse::class.java)

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {

    }

    @Test
    fun `RegisterResponse Success, from JSON to Object`() {
        with(AuthTestUtil) {
            val responseSuccess = moshiAdapter.fromJson(registerResponseSuccessJson)

            assert(registerResponseSuccess == responseSuccess)
        }
    }

    @Test
    fun `RegisterResponse Failed, from JSON to Object`() {
        with(AuthTestUtil) {
            val responseFailed = moshiAdapter.fromJson(registerResponseFailedJson)

            assert(registerResponseFailed == responseFailed)
        }
    }
}
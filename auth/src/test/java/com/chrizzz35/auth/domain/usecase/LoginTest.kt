package com.chrizzz35.auth.domain.usecase

import com.chrizzz35.auth.util.AuthTestUtil
import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.auth.rule.RxSchedulerRule
import com.chrizzz35.common.http.HttpFailedException
import com.chrizzz35.common.extension.toMD5
import io.reactivex.Completable
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class LoginTest {

    @get:Rule
    val rxRule: TestRule = RxSchedulerRule()

    @Mock
    lateinit var authRepository: AuthRepository
    lateinit var login: Login

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        login = Login(authRepository)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Execute Login, return success`() {
        with(AuthTestUtil) {
            // Given
            Mockito.`when`(authRepository.login(username, password.toMD5()))
                    .thenReturn(Completable.complete())

            // When
            val testObserver = login.execute(Login.Param(username, password)).test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertComplete()

            Mockito.verify(authRepository).login(username, password.toMD5())
        }
    }

    @Test
    fun `Execute Login, return failed`() {
        with(AuthTestUtil) {
            // Given
            Mockito.`when`(authRepository.login(username, password.toMD5()))
                    .thenReturn(Completable.error(HttpFailedException(loginResponseFailed.message)))

            // When
            val testObserver = login.execute(Login.Param(username, password)).test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertError {
                it is HttpFailedException
                        && it.message == loginResponseFailed.message
            }

            Mockito.verify(authRepository).login(username, password.toMD5())
        }
    }
}
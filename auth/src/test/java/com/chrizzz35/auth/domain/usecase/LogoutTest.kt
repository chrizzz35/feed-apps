package com.chrizzz35.auth.domain.usecase

import com.chrizzz35.auth.util.AuthTestUtil
import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.auth.rule.RxSchedulerRule
import com.chrizzz35.common.http.HttpFailedException
import io.reactivex.Completable
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class LogoutTest {

    @get:Rule
    val rxRule: TestRule = RxSchedulerRule()

    @Mock
    lateinit var authRepository: AuthRepository
    lateinit var logout: Logout

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        logout = Logout(authRepository)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Execute Logout, return success`() {
        with(AuthTestUtil) {
            // Given
            Mockito.`when`(authRepository.logout()).thenReturn(Completable.complete())

            // When
            val testObserver = logout.execute(Logout.Param()).test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertComplete()
        }
    }

    @Test
    fun `Execute Logout, return failed`() {
        with(AuthTestUtil) {
            // Given
            Mockito.`when`(authRepository.logout()).thenReturn(Completable.error(HttpFailedException(messageFailed)))

            // When
            val testObserver = logout.execute(Logout.Param()).test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertError {
                it is HttpFailedException
                        && it.message == messageFailed
            }
        }
    }
}
package com.chrizzz35.auth.domain.usecase

import com.chrizzz35.auth.util.AuthTestUtil
import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.auth.rule.RxSchedulerRule
import com.chrizzz35.common.http.HttpFailedException
import com.chrizzz35.common.extension.toMD5
import io.reactivex.Completable
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class RegisterTest {

    @get:Rule
    val rxRule: TestRule = RxSchedulerRule()

    @Mock
    lateinit var authRepository: AuthRepository
    lateinit var register: Register

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        register = Register(authRepository)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Execute Register, return success`() {
        with(AuthTestUtil) {
            // Given
            Mockito.`when`(authRepository.register(name, phone, email, gender, dob, password.toMD5()))
                    .thenReturn(Completable.complete())

            // When
            val testObserver = register.execute(Register.Param(name, phone, email, gender, dob, password)).test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertComplete()
        }
    }

    @Test
    fun `Execute Register, return failed`() {
        with(AuthTestUtil) {
            // Given
            Mockito.`when`(authRepository.register(name, phone, email, gender, dob, password.toMD5()))
                    .thenReturn(Completable.error(HttpFailedException(registerResponseFailed.message)))

            // When
            val testObserver = register.execute(Register.Param(name, phone, email, gender, dob, password)).test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertError {
                it is HttpFailedException
                        && it.message == registerResponseFailed.message
            }
        }
    }
}
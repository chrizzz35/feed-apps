package com.chrizzz35.auth.presentation.auth.login

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.chrizzz35.auth.util.AuthTestUtil
import com.chrizzz35.auth.domain.repository.AuthRepository
import com.chrizzz35.auth.domain.usecase.Login
import com.chrizzz35.auth.rule.RxSchedulerRule
import com.chrizzz35.common.http.HttpFailedException
import com.chrizzz35.common.extension.toMD5
import io.reactivex.Completable
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.*
import java.util.concurrent.TimeoutException

class LoginPresenterTest {

    @get:Rule
    val rxRule: TestRule = RxSchedulerRule()

    @get:Rule
    val liveDataRule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var authRepository: AuthRepository

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var login: Login
    private lateinit var loginPresenter: LoginPresenter

    @Mock
    lateinit var observerLoginLoading: Observer<Boolean>
    @Mock
    lateinit var observerLoginSuccess: Observer<Boolean>
    @Mock
    lateinit var observerLoginFailed: Observer<LoginMessageType>

    @Captor
    lateinit var captorLoginLoading : ArgumentCaptor<Boolean>
    @Captor
    lateinit var captorLoginSuccess : ArgumentCaptor<Boolean>
    @Captor
    lateinit var captorLoginFailed : ArgumentCaptor<LoginMessageType>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        loginViewModel = LoginViewModel()
        login = Login(authRepository)
        loginPresenter = LoginPresenter(loginViewModel, login)

        loginViewModel.loginLoading.observeForever(observerLoginLoading)
        loginViewModel.loginSuccess.observeForever(observerLoginSuccess)
        loginViewModel.loginFailed.observeForever(observerLoginFailed)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Login Success`() {
        with(AuthTestUtil) {
            // Given
            loginViewModel.username.value = username
            loginViewModel.password.value = password
            Mockito.`when`(authRepository.login(username, password.toMD5())).thenReturn(Completable.complete())

            // When
            loginPresenter.login()

            // Then
            captorLoginLoading.run {
                Mockito.verify(observerLoginLoading, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == true)
                assert(endState == false)
            }

            captorLoginSuccess.run {
                Mockito.verify(observerLoginSuccess, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == null)
                assert(endState == true)
            }

            captorLoginFailed.run {
                Mockito.verify(observerLoginFailed, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == null)
                assert(endState == null)
            }
        }
    }

    @Test
    fun `Login Failed`() {
        with(AuthTestUtil) {
            // Given
            loginViewModel.username.value = username
            loginViewModel.password.value = password
            Mockito.`when`(authRepository.login(username, password.toMD5())).thenReturn(Completable.error(HttpFailedException(messageFailed)))

            // When
            loginPresenter.login()

            // Then
            captorLoginLoading.run {
                Mockito.verify(observerLoginLoading, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == true)
                assert(endState == false)
            }

            captorLoginSuccess.run {
                Mockito.verify(observerLoginSuccess, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == null)
                assert(endState == null)
            }

            captorLoginFailed.run {
                Mockito.verify(observerLoginFailed, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == null)
                assert(endState is LoginMessageType.ApiFailed)
            }
        }
    }

    @Test
    fun `Login Failed, Connection Failure`() {
        with(AuthTestUtil) {
            // Given
            loginViewModel.username.value = username
            loginViewModel.password.value = password
            Mockito.`when`(authRepository.login(username, password.toMD5())).thenReturn(Completable.error(TimeoutException(messageFailed)))

            // When
            loginPresenter.login()

            // Then
            captorLoginLoading.run {
                Mockito.verify(observerLoginLoading, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == true)
                assert(endState == false)
            }

            captorLoginSuccess.run {
                Mockito.verify(observerLoginSuccess, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == null)
                assert(endState == null)
            }

            captorLoginFailed.run {
                Mockito.verify(observerLoginFailed, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == null)
                assert(endState is LoginMessageType.NetworkFailed)
            }
        }
    }

    @Test
    fun `Login Failed, Username Empty`() {
        with(AuthTestUtil) {
            // Given
            loginViewModel.username.value = ""
            loginViewModel.password.value = password

            // When
            loginPresenter.login()

            // Then
            captorLoginLoading.run {
                Mockito.verify(observerLoginLoading, Mockito.never()).onChanged(capture())
            }

            captorLoginSuccess.run {
                Mockito.verify(observerLoginSuccess, Mockito.never()).onChanged(capture())
            }

            captorLoginFailed.run {
                Mockito.verify(observerLoginFailed, Mockito.times(1)).onChanged(capture())
                val (endState) = allValues

                assert(endState is LoginMessageType.UsernameEmpty)
            }
        }
    }

    @Test
    fun `Login Failed, Password Empty`() {
        with(AuthTestUtil) {
            // Given
            loginViewModel.username.value = username
            loginViewModel.password.value = ""

            // When
            loginPresenter.login()

            // Then
            captorLoginLoading.run {
                Mockito.verify(observerLoginLoading, Mockito.never()).onChanged(capture())
            }

            captorLoginSuccess.run {
                Mockito.verify(observerLoginSuccess, Mockito.never()).onChanged(capture())
            }

            captorLoginFailed.run {
                Mockito.verify(observerLoginFailed, Mockito.times(1)).onChanged(capture())
                val (endState) = allValues

                assert(endState is LoginMessageType.PasswordEmpty)
            }
        }
    }
}
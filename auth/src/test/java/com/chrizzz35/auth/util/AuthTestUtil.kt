package com.chrizzz35.auth.util

import com.chrizzz35.auth.data.remote.request.LoginRequest
import com.chrizzz35.auth.data.remote.request.RegisterRequest
import com.chrizzz35.auth.data.remote.response.LoginResponse
import com.chrizzz35.auth.data.remote.response.LoginResponseData
import com.chrizzz35.auth.data.remote.response.RegisterResponse
import com.chrizzz35.auth.data.remote.response.RegisterResponseData
import com.chrizzz35.auth.domain.entity.User
import com.chrizzz35.common.config.NetworkConfig

object AuthTestUtil {

    // User
    val id = 1
    val email = "chrizzz35@yahoo.co.id"
    val phone = "081287991931"
    val name = "Christian Leonard"
    val gender = 1
    val dob = "1992-10-30"

    val username = phone
    val password = "123456"

    val user = User(id, name, username, email, gender, dob, null, null)

    val userList: List<User>
    get() {
        val list = arrayListOf(user)

        for(index in 2 until 20) {
            list += User(index, "Dummy $index", username, email, gender, dob, null, null)
        }

        return list
    }


    // Login
    val loginRequest = LoginRequest(username, password)

    val tokenSuccess = "1234567890"

    val messageSuccess = "Success"
    val messageFailed = "Failed"

    val loginResponseSuccess = LoginResponse(NetworkConfig.API_SUCCESS,messageSuccess,
            LoginResponseData(id, name, phone, email, gender, dob, null, null, tokenSuccess))
    val loginResponseFailed = LoginResponse(NetworkConfig.API_FAILED, messageFailed,null)

    val loginRequestJson =
            "{" +
                "\"username\":\"$username\"," +
                "\"password\":\"$password\"" +
            "}"

    val loginResponseSuccessJson =
            "{" +
                "\"status_code\":${NetworkConfig.API_SUCCESS}," +
                "\"message\":\"$messageSuccess\"," +
                "\"data\":{" +
                    "\"id\":$id," +
                    "\"name\":\"$name\"," +
                    "\"phone\":\"$phone\"," +
                    "\"email\":\"$email\"," +
                    "\"gender\":$gender," +
                    "\"birth_date\":\"$dob\"," +
                    "\"photo\":null," +
                    "\"cover\":null," +
                    "\"data\":\"$tokenSuccess\"" +
                "}," +
                "\"exception\":null" +
            "}"

    val loginResponseFailedJson =
            "{" +
                "\"status_code\":${NetworkConfig.API_FAILED}," +
                "\"message\":\"$messageFailed\"," +
                "\"data\":null," +
                "\"exception\":null" +
            "}"

    // Register
    val registerRequest = RegisterRequest(name, phone, email, gender, dob, password)

    val registerResponseSuccess = RegisterResponse(NetworkConfig.API_SUCCESS,messageSuccess,
            RegisterResponseData(id, name, phone, email, gender, dob))
    val registerResponseFailed = RegisterResponse(NetworkConfig.API_FAILED, messageFailed, null)

    val registerRequestJson =
            "{" +
                "\"name\":\"$name\"," +
                "\"phone\":\"$phone\"," +
                "\"email\":\"$email\"," +
                "\"gender\":$gender," +
                "\"birth_date\":\"$dob\"," +
                "\"password\":\"$password\"" +
            "}"

    val registerResponseSuccessJson =
            "{" +
                "\"status_code\":${NetworkConfig.API_SUCCESS}," +
                "\"message\":\"$messageSuccess\"," +
                "\"data\":{" +
                    "\"name\":\"$name\"," +
                    "\"phone\":\"$phone\"," +
                    "\"email\":\"$email\"," +
                    "\"gender\":$gender," +
                    "\"birth_date\":\"$dob\"," +
                    "\"id\":$id" +
                "}," +
                "\"exception\":null" +
            "}"

    val registerResponseFailedJson =
            "{" +
                "\"status_code\":${NetworkConfig.API_FAILED}," +
                "\"message\":\"$messageFailed\"," +
                "\"data\":null," +
                "\"exception\":null" +
            "}"
}
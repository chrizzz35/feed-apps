package com.chrizzz35.common.base

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()

        if(!compositeDisposable.isDisposed) compositeDisposable.clear()
    }
}
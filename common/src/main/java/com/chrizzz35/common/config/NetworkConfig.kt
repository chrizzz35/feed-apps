package com.chrizzz35.common.config

class NetworkConfig {
    companion object {
        const val BASE_URL = "http://api.angelina.web.id/"

        const val API_SUCCESS = 200
        const val API_FAILED = 400
        const val API_SESSION_EXPIRED = 401

        const val TOKEN_PREFERENCES = "token_preferences"
    }
}
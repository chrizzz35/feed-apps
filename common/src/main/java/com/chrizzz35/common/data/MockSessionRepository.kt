package com.chrizzz35.common.data

import com.chrizzz35.common.domain.repository.SessionRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class MockSessionRepository @Inject constructor(): SessionRepository {

    override fun <T : Any> validateSessionSingle(single: Single<T>): Single<T> {
        return single
    }

    override fun refreshSessionToken(): Completable {
        return Completable.complete()
    }

    override fun getSessionToken(): Single<String> {
        return Single.just("")
    }

    override fun saveSessionToken(token: String): Completable {
        return Completable.complete()
    }
}
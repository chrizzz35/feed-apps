package com.chrizzz35.common.data

import com.chrizzz35.common.data.local.SessionLocalRepository
import com.chrizzz35.common.data.remote.SessionRemoteRepository
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.common.http.HttpSessionExpiredException
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class SessionRepositoryImpl @Inject constructor(
        private val sessionLocalRepository: SessionLocalRepository,
        private val sessionRemoteRepository: SessionRemoteRepository
) : SessionRepository {

    override fun <T : Any> validateSessionSingle(single: Single<T>): Single<T> {
        return single.retry { _: Int, throwable: Throwable ->
            if(throwable is HttpSessionExpiredException) {
                return@retry refreshSessionToken()
                        .toSingleDefault(true)
                        .onErrorReturnItem(false)
                        .blockingGet()
            }

            return@retry false
        }
    }

    override fun refreshSessionToken(): Completable {
        return sessionRemoteRepository.refreshSessionToken()
                .retry { times, _ -> times < 3 }
                .flatMapCompletable {
                    return@flatMapCompletable saveSessionToken(it)
                }
    }

    override fun getSessionToken(): Single<String> {
        return sessionLocalRepository.getSessionToken()
    }

    override fun saveSessionToken(token: String): Completable {
        return sessionLocalRepository.saveSessionToken(token)
    }
}
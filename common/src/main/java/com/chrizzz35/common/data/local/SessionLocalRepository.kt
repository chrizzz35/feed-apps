package com.chrizzz35.common.data.local

import io.reactivex.Completable
import io.reactivex.Single

interface SessionLocalRepository {
    fun getSessionToken() : Single<String>
    fun saveSessionToken(token: String) : Completable
}
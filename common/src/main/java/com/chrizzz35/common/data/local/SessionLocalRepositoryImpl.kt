package com.chrizzz35.common.data.local

import android.content.SharedPreferences
import com.chrizzz35.common.config.NetworkConfig
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class SessionLocalRepositoryImpl @Inject constructor(
        private val sharedPreferences: SharedPreferences
): SessionLocalRepository {
    override fun getSessionToken(): Single<String> {
        return Single.just(sharedPreferences.getString(NetworkConfig.TOKEN_PREFERENCES, ""))
    }

    override fun saveSessionToken(token: String): Completable {
        return Completable.create {
            try {
                sharedPreferences.edit()
                        .putString(NetworkConfig.TOKEN_PREFERENCES, token)
                        .apply()

                it.onComplete()
            }
            catch (e: Exception) {
                it.onError(e)
            }
        }
    }
}
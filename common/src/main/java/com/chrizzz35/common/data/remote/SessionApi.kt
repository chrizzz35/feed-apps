package com.chrizzz35.common.data.remote

import com.chrizzz35.common.data.remote.response.RefreshSessionResponse
import io.reactivex.Single
import retrofit2.http.POST

interface SessionApi {

    @POST("/api/auth/refreshToken")
    fun refreshToken() : Single<RefreshSessionResponse>
}
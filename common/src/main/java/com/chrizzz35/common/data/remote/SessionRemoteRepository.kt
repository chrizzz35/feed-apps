package com.chrizzz35.common.data.remote

import io.reactivex.Single

interface SessionRemoteRepository {
    fun refreshSessionToken() : Single<String>
}
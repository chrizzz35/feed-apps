package com.chrizzz35.common.data.remote

import com.chrizzz35.common.http.HttpParser
import io.reactivex.Single
import javax.inject.Inject

class SessionRemoteRepositoryImpl @Inject constructor(
        private val sessionApi: SessionApi,
        private val httpParser: HttpParser
): SessionRemoteRepository {
    override fun refreshSessionToken(): Single<String> {
        return sessionApi.refreshToken().map { httpParser.parseResponse(it) }
    }
}
package com.chrizzz35.common.dialog.dobpicker

import android.app.DatePickerDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import java.util.*

class DobPickerDialogFragment : BottomSheetDialogFragment() {

    private var listener: DobPickerDialogFragmentListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val onDateSelectedListener = DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
            listener?.onDobSelected("$selectedYear-$selectedMonth-$selectedDay")
        }

        return DatePickerDialog(context!!, android.R.style.Theme_Holo_Light_Dialog, onDateSelectedListener, year, month, day).apply {
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    fun setDobPickerDialogFragmentListener(dobPickerDialogFragmentListener: DobPickerDialogFragmentListener) {
        listener = dobPickerDialogFragmentListener
    }

    interface DobPickerDialogFragmentListener {
        fun onDobSelected(selectedDob: String)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                DobPickerDialogFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}

package com.chrizzz35.common.dialog.genderpicker

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.chrizzz35.common.R
import com.chrizzz35.common.databinding.ItemGenderPickerDialogBinding

class GenderPickerDialogAdapter(
        private val genderList: List<Int>,
        private val onItemSelected: (Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, itemViewType: Int): RecyclerView.ViewHolder =
            ItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_gender_picker_dialog, parent, false))

    override fun getItemCount(): Int = genderList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is ItemViewHolder) {
            viewHolder.bind(genderList[position], onItemSelected)
        }
    }

    class ItemViewHolder(private val binding: ItemGenderPickerDialogBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(gender: Int, onItemSelected: (Int) -> Unit) {
            binding.gender = gender
            itemView.setOnClickListener { onItemSelected(gender) }
        }
    }
}
package com.chrizzz35.common.dialog.genderpicker

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.common.R
import kotlinx.android.synthetic.main.fragment_gender_picker_dialog.view.*

class GenderPickerDialogFragment : BottomSheetDialogFragment() {

    private var listData: List<Int> = arrayListOf(1, 0)
    private var listener: GenderPickerDialogFragmentListener? = null

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var genderPickerDialogAdapter: GenderPickerDialogAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_gender_picker_dialog, container, false)

        layoutManager = LinearLayoutManager(context)
        genderPickerDialogAdapter = GenderPickerDialogAdapter(listData) {
            listener?.onGenderSelected(it)
            dismiss()
        }

        view.rv_item.layoutManager = layoutManager
        view.rv_item.adapter = genderPickerDialogAdapter
        view.rv_item.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        view.btn_close.setOnClickListener { dismiss() }

        return view
    }

    fun setPickerDialogFragmentListener(genderPickerDialogFragmentListener: GenderPickerDialogFragmentListener) {
        this.listener = genderPickerDialogFragmentListener
    }

    interface GenderPickerDialogFragmentListener {
        fun onGenderSelected(selectedGender: Int)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                GenderPickerDialogFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}

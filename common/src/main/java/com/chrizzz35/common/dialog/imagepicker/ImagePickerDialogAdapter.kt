package com.chrizzz35.common.dialog.imagepicker

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.chrizzz35.common.R
import com.chrizzz35.common.databinding.ItemImagePickerDialogBinding

class ImagePickerDialogAdapter (
        private val stringList: List<String>,
        private val onStringSelected: (String) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, itemViewType: Int): RecyclerView.ViewHolder =
            StringViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_image_picker_dialog, parent, false))

    override fun getItemCount(): Int =
            stringList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is StringViewHolder) {
            viewHolder.bind(stringList[position], onStringSelected)
        }
    }

    class StringViewHolder(private val binding: ItemImagePickerDialogBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(string: String, onStringSelected: (String) -> Unit) {
            binding.string = string
            itemView.setOnClickListener { onStringSelected(string) }
        }
    }
}
package com.chrizzz35.common.dialog.imagepicker

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.content.FileProvider
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.common.R
import kotlinx.android.synthetic.main.fragment_image_picker_dialog.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

private const val TYPE_IMAGE_PICKER_CAMERA = "Camera"
private const val TYPE_IMAGE_PICKER_GALLERY = "Gallery"

private const val REQUEST_CODE_CAMERA = 0
private const val REQUEST_CODE_GALLERY = 1

class StringPickerDialogFragment : BottomSheetDialogFragment() {

    private var imageFile: File? = null
    private val imagePickerType = arrayListOf(
            TYPE_IMAGE_PICKER_CAMERA,
            TYPE_IMAGE_PICKER_GALLERY
    )

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var adapter: ImagePickerDialogAdapter

    private var listener: StringPickerDialogFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_image_picker_dialog, container, false)

        layoutManager = LinearLayoutManager(context)
        adapter = ImagePickerDialogAdapter(imagePickerType) {
            when(it) {
                TYPE_IMAGE_PICKER_CAMERA -> openCamera()
                TYPE_IMAGE_PICKER_GALLERY -> openGallery()
            }
        }

        view.rv_string.layoutManager = layoutManager
        view.rv_string.adapter = adapter

        return view
    }

    private fun createTempFile() : File? {
        return try {
            val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
            val storageDir: File = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!

            File.createTempFile("tmp_$timeStamp", ".jpg", storageDir)
        } catch (ex: IOException) {
            null
        }
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(activity!!.packageManager)?.also {
                imageFile = createTempFile()

                imageFile?.also { file ->
                    val photoURI: Uri = FileProvider.getUriForFile(context!!, "com.chrizzz35.myapp.fileprovider", file)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_CODE_CAMERA)
                }
            }
        }
    }

    private fun openGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            startActivityForResult(Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "image/*"
            }, REQUEST_CODE_GALLERY)
        } else {
            startActivityForResult(Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI), REQUEST_CODE_GALLERY)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_CODE_CAMERA -> {
                if(resultCode == Activity.RESULT_OK) {
                    imageFile?.let {
                        listener?.onImagePicked(it)
                    }
                }
            }
            REQUEST_CODE_GALLERY -> {
                if(resultCode == Activity.RESULT_OK) {
                    val uri = data?.data
                    uri?.also {
                        imageFile = createTempFile()

                        imageFile?.also { file ->
                            val fileOutputStream = FileOutputStream(file)
                            val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, uri)
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
                            fileOutputStream.flush()
                            fileOutputStream.close()
                        }
                    }

                    imageFile?.let {
                        listener?.onImagePicked(it)
                    }
                }
            }
        }

        dismiss()
    }

    fun setStringPickerDialogFragmentListener(stringPickerDialogFragmentListener: StringPickerDialogFragmentListener) {
        listener = stringPickerDialogFragmentListener
    }

    interface StringPickerDialogFragmentListener {
        fun onImagePicked(file: File)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                StringPickerDialogFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}

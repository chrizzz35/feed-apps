package com.chrizzz35.common.dialog.optionmenu

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.chrizzz35.common.databinding.ItemOptionMenuDialogBinding

class OptionMenuDialogAdapter (
        private val menuList: List<String>,
        private val onMenuSelected: (Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, itemViewType: Int): RecyclerView.ViewHolder =
            MenuViewHolder(ItemOptionMenuDialogBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = menuList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is MenuViewHolder) {
            viewHolder.bind(menuList[position], position, onMenuSelected)
        }
    }

    class MenuViewHolder(private val binding: ItemOptionMenuDialogBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(menu: String, position: Int, onMenuSelected: (Int) -> Unit) {
            binding.menu = menu
            itemView.setOnClickListener { onMenuSelected(position) }
        }
    }
}
package com.chrizzz35.common.dialog.optionmenu

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.chrizzz35.common.R
import kotlinx.android.synthetic.main.fragment_option_menu_dialog.*

private const val ARG_MENU_LIST = "arg_menu_list"

class OptionMenuDialogFragment : DialogFragment() {

    private var menuList: List<String>? = null
    private var listener: OptionMenuDialogFragmentListener? = null

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var optionMenuDialogAdapter: OptionMenuDialogAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            menuList = it.getStringArrayList(ARG_MENU_LIST)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_option_menu_dialog, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupDialogWindow()
        setupMenuList()
    }

    private fun setupDialogWindow() {
        dialog.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.TOP)
        dialog.window?.attributes?.windowAnimations = R.style.OptionMenuDialogAnimation
    }

    private fun setupMenuList() {
        layoutManager = LinearLayoutManager(context)
        optionMenuDialogAdapter = OptionMenuDialogAdapter(menuList!!) {
            listener?.onSelectedMenu(it)
        }

        rv_menu.layoutManager = layoutManager
        rv_menu.adapter = optionMenuDialogAdapter
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OptionMenuDialogFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OptionMenuDialogFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OptionMenuDialogFragmentListener {
        fun onSelectedMenu(position: Int)
    }

    companion object {
        @JvmStatic
        fun newInstance(menuList: List<String>) =
                OptionMenuDialogFragment().apply {
                    arguments = Bundle().apply {
                        putStringArrayList(ARG_MENU_LIST, ArrayList(menuList))
                    }
                }
    }
}

package com.chrizzz35.common.domain

import io.reactivex.Completable

interface CompletableUseCase<Param> {
    fun execute(param: Param) : Completable
}
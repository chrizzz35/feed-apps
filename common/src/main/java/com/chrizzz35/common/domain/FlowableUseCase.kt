package com.chrizzz35.common.domain

import io.reactivex.Flowable

interface FlowableUseCase<Param, Result> {
    fun execute(param: Param) : Flowable<Result>
}
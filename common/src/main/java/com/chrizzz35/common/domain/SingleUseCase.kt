package com.chrizzz35.common.domain

import io.reactivex.Single

interface SingleUseCase<Param, Result> {
    fun execute(param: Param) : Single<Result>
}
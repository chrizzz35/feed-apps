package com.chrizzz35.common.domain.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface SessionRepository {
    fun <T: Any> validateSessionSingle(single: Single<T>) : Single<T>
    fun refreshSessionToken() : Completable

    fun getSessionToken() : Single<String>
    fun saveSessionToken(token: String) : Completable
}
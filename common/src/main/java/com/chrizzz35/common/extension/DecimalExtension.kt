package com.chrizzz35.common.extension

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun Double.numberFormat(decimalCount: Int = 0) : String {
    val decimalFormatSymbols = DecimalFormatSymbols(Locale.US)
    decimalFormatSymbols.decimalSeparator = '.'
    decimalFormatSymbols.groupingSeparator = ','

    var pattern = "#,###"
    for(index in 0 .. decimalCount) {
        if(index == 0) pattern += "."

        pattern += "#"
    }

    val decimalFormat = DecimalFormat(pattern, decimalFormatSymbols)
    decimalFormat.minimumFractionDigits = decimalCount
    decimalFormat.maximumFractionDigits = decimalCount
    return decimalFormat.format(this)
}
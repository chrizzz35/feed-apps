package com.chrizzz35.common.extension

import android.content.Context

fun Int.pxToDp(context: Context) : Int {
    val density = context.resources.displayMetrics.density

    return (this / density).toInt()
}

fun Int.pxToSp(context: Context) : Float {
    val density = context.resources.displayMetrics.scaledDensity

    return this / density
}

fun Int.dpToPx(context: Context) : Int {
    val density = context.resources.displayMetrics.density

    return (this * density).toInt()
}

fun Int.spToPx(context: Context) : Float {
    val density = context.resources.displayMetrics.scaledDensity

    return this * density
}

fun Context.getScreenWidth() : Int {
    return resources.displayMetrics.widthPixels
}

fun Context.getScreenHeight() : Int {
    return resources.displayMetrics.heightPixels
}
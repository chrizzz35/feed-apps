package com.chrizzz35.common.extension

import android.util.Patterns
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*

fun String.isValidEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isValidPhone(): Boolean {
    return Patterns.PHONE.matcher(this).matches()
}

fun String.dateFormat(oldFormat: String, newFormat: String): String {
    var result = ""

    val input = SimpleDateFormat(oldFormat, Locale.US)
    val output = SimpleDateFormat(newFormat, Locale.US)
    try {
        val temp = input.parse(this)
        result = output.format(temp)
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return result
}

fun String.toMD5(): String {
    val md5 = "MD5"
    try {
        // Create MD5 Hash
        val digest = java.security.MessageDigest
                .getInstance(md5)
        digest.update(toByteArray())
        val messageDigest = digest.digest()

        // Create Hex String
        val hexString = StringBuilder()
        for (aMessageDigest in messageDigest) {
            var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
            while (h.length < 2)
                h = "0$h"
            hexString.append(h)
        }
        return hexString.toString().toUpperCase()

    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    }

    return ""
}
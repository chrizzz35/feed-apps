package com.chrizzz35.common.extension

import android.databinding.BindingAdapter
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import com.chrizzz35.common.R

@BindingAdapter("visibility")
fun View.setVisibility(visible: Boolean) {
    visibility = if(visible) View.VISIBLE else View.GONE
}

@BindingAdapter("error")
fun TextInputLayout.setErrorMessage(message: String?) {
    message?.let {
        isErrorEnabled = true
        error = message
    }
}

@BindingAdapter("url", "signature")
fun ImageView.loadImageUrl(url: String, signature: String = "") {
    Glide.with(context)
            .setDefaultRequestOptions(
                    RequestOptions()
                            .placeholder(ContextCompat.getDrawable(context, R.drawable.ic_placeholder))
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .signature(ObjectKey(signature))
                            .error(null)
            )
            .load(url)
            .into(this)
}
package com.chrizzz35.common.http

class HttpFailedException(message: String?) : Exception(message)
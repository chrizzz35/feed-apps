package com.chrizzz35.common.http

import com.chrizzz35.common.config.NetworkConfig
import io.reactivex.Completable

class HttpParser {
    fun <T> parseResponse(response: HttpResponse<T>): T {
        return with(response) {
            when(statusCode) {
                NetworkConfig.API_SUCCESS -> data!!
                NetworkConfig.API_SESSION_EXPIRED -> throw HttpSessionExpiredException(message)
                else -> throw HttpFailedException(message)
            }
        }
    }

    fun <T> parseDefaultResponse(response: HttpResponse<T>): Completable {
        return with(response) {
            when(statusCode) {
                NetworkConfig.API_SUCCESS -> Completable.complete()
                NetworkConfig.API_SESSION_EXPIRED -> throw HttpSessionExpiredException(message)
                else -> throw HttpFailedException(message)
            }
        }
    }
}
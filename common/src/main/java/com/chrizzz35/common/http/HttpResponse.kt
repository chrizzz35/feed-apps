package com.chrizzz35.common.http

abstract class HttpResponse<T> {
    abstract val statusCode: Int?
    abstract val message: String?
    abstract val data: T?
}
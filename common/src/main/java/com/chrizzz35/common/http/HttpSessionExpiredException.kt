package com.chrizzz35.common.http

class HttpSessionExpiredException(message: String?) : Exception(message)
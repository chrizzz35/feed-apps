package com.chrizzz35.common.navigator

import android.app.Activity

interface HomeNavigator {
    fun redirectHome(activity: Activity)
}
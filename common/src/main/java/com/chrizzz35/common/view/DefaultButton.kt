package com.chrizzz35.common.view

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.widget.Button
import com.chrizzz35.common.R
import com.chrizzz35.common.extension.dpToPx

class DefaultButton : Button {
    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
        background = ContextCompat.getDrawable(context, R.drawable.background_default_button)
        setTextColor(Color.WHITE)
        textSize = 15.0f
        minHeight = 50.dpToPx(context)
    }
}
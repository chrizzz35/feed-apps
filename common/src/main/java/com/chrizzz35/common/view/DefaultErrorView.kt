package com.chrizzz35.common.view

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.chrizzz35.common.R
import com.chrizzz35.common.extension.dpToPx

class DefaultErrorView : LinearLayout {
    constructor(context: Context?) : super(context) {
        init(context!!)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context!!, attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context!!, attrs)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context!!, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet? = null) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.DefaultErrorView)
        val text = typedArray.getString(R.styleable.DefaultErrorView_text)

        orientation = VERTICAL

        addView(ImageView(context).apply {
            setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_failed))
            layoutParams = LayoutParams(50.dpToPx(context), 50.dpToPx(context))
            gravity = Gravity.CENTER
        })

        addView(TextView(context).apply {
            setText(text)
            gravity = Gravity.CENTER
            LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT).also {
                it.topMargin = 10.dpToPx(context)
            }
        })

        typedArray.recycle()
    }
}
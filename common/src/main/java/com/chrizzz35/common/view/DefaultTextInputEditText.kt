package com.chrizzz35.common.view

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.util.AttributeSet
import com.chrizzz35.common.extension.dpToPx

class DefaultTextInputEditText : TextInputEditText {
    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        minHeight = 50.dpToPx(context)
        textSize = 15.0f
    }
}
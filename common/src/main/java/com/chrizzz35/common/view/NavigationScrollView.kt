package com.chrizzz35.common.view

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ScrollView
import dagger.internal.Beta

@Beta
//class NavigationScrollView : NestedScrollView {
class NavigationScrollView : ScrollView {

    private val customMaxOverScrollY = 50
    private var overScrollUpListener: OverScrollUpListener? = null
    private var overScrollDownListener: OverScrollDownListener? = null

//    constructor(context: Context) : super(context) {
//        init()
//    }
//
//    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
//        init()
//    }
//
//    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//        init()
//    }

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
        overScrollMode = OVER_SCROLL_ALWAYS
    }

    override fun overScrollBy(deltaX: Int, deltaY: Int, scrollX: Int, scrollY: Int, scrollRangeX: Int, scrollRangeY: Int, maxOverScrollX: Int, maxOverScrollY: Int, isTouchEvent: Boolean): Boolean {
        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, customMaxOverScrollY, isTouchEvent)
    }

    fun setOverScrollUpListener(overScrollUpListener: OverScrollUpListener) {
        this.overScrollUpListener = overScrollUpListener
    }

    fun setOverScrollDownListener(overScrollDownListener: OverScrollDownListener) {
        this.overScrollDownListener = overScrollDownListener
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        when(ev?.action) {
            MotionEvent.ACTION_UP -> {
                val childHeight = getChildAt(0).height
                val scrollViewHeight = height - paddingTop - paddingBottom

                val scrollableHeight = if(childHeight <= scrollViewHeight) customMaxOverScrollY
                else childHeight - scrollViewHeight + customMaxOverScrollY

                if(scrollY == (-1*customMaxOverScrollY)) {
                    overScrollUpListener?.onOverScrollUp()
                    smoothScrollTo(0, 0)
                }
                else if(scrollY == scrollableHeight) {
                    overScrollDownListener?.onOverScrollDown()
                    smoothScrollTo(0, scrollableHeight)
                }
            }
        }

        return super.onTouchEvent(ev)
    }

    override fun onOverScrolled(scrollX: Int, scrollY: Int, clampedX: Boolean, clampedY: Boolean) {
        if(scrollY < 0 && overScrollUpListener != null) {
            super.onOverScrolled(scrollX, scrollY, clampedX, clampedY)
        }

        if(scrollY > 0 && overScrollDownListener != null) {
            super.onOverScrolled(scrollX, scrollY, clampedX, clampedY)
        }
    }

    interface OverScrollUpListener {
        fun onOverScrollUp()
    }

    interface OverScrollDownListener {
        fun onOverScrollDown()
    }
}
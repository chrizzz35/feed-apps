package com.chrizzz35.common.data

import com.chrizzz35.common.data.local.SessionLocalRepository
import com.chrizzz35.common.data.remote.SessionRemoteRepositoryImpl
import com.chrizzz35.common.data.remote.SessionRemoteRepository
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.common.http.HttpFailedException
import com.chrizzz35.common.http.HttpParser
import com.chrizzz35.common.mock.MockAttemptSessionApi
import com.chrizzz35.common.util.SessionTestUtil
import io.reactivex.Completable
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

class SessionRepositoryImplTest {

    lateinit var mockAttemptSessionApi: MockAttemptSessionApi

    val httpParser: HttpParser = HttpParser()

    @Mock
    lateinit var sessionLocalRepository: SessionLocalRepository
    lateinit var sessionRemoteRepository: SessionRemoteRepository
    lateinit var sessionRepository: SessionRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Refresh token, return failed 3 times, no session saved`() {
        with(SessionTestUtil) {
            // Given
            val successOn = 4
            mockAttemptSessionApi = MockAttemptSessionApi(successOn)
            sessionRemoteRepository = SessionRemoteRepositoryImpl(mockAttemptSessionApi, httpParser)
            sessionRepository = SessionRepositoryImpl(sessionLocalRepository, sessionRemoteRepository)

            // When
            val testObserver = sessionRepository.refreshSessionToken().test()

            // Then
            // testObserver.awaitTerminalEvent()
            testObserver.assertError {
                it is HttpFailedException && it.message == refreshSessionResponseFailed.message
            }

            Mockito.verify(sessionLocalRepository, Mockito.never()).saveSessionToken(token)
        }
    }

    @Test
    fun `Refresh token, return failed 2 times, return success on 3rd, then save session`() {
        with(SessionTestUtil) {
            // Given
            val successOn = 3
            mockAttemptSessionApi = MockAttemptSessionApi(successOn)
            sessionRemoteRepository = SessionRemoteRepositoryImpl(mockAttemptSessionApi, httpParser)
            sessionRepository = SessionRepositoryImpl(sessionLocalRepository, sessionRemoteRepository)

            Mockito.`when`(sessionLocalRepository.saveSessionToken(token)).thenReturn(Completable.complete())

            // When
            val testObserver = sessionRepository.refreshSessionToken().test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertComplete()

            Mockito.verify(sessionLocalRepository, Mockito.only()).saveSessionToken(token)
            assert(mockAttemptSessionApi.attempt == successOn)
        }
    }

    @Test
    fun `Refresh token, return failed 1 times, return success on 2nd, then save session`() {
        with(SessionTestUtil) {
            // Given
            val successOn = 2
            mockAttemptSessionApi = MockAttemptSessionApi(successOn)
            sessionRemoteRepository = SessionRemoteRepositoryImpl(mockAttemptSessionApi, httpParser)
            sessionRepository = SessionRepositoryImpl(sessionLocalRepository, sessionRemoteRepository)

            Mockito.`when`(sessionLocalRepository.saveSessionToken(token)).thenReturn(Completable.complete())

            // When
            val testObserver = sessionRepository.refreshSessionToken().test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertComplete()

            Mockito.verify(sessionLocalRepository, Mockito.only()).saveSessionToken(token)
            assert(mockAttemptSessionApi.attempt == successOn)
        }
    }

    @Test
    fun `Refresh token, return success, then save session`() {
        with(SessionTestUtil) {
            // Given
            val successOn = 1
            mockAttemptSessionApi = MockAttemptSessionApi(successOn)
            sessionRemoteRepository = SessionRemoteRepositoryImpl(mockAttemptSessionApi, httpParser)
            sessionRepository = SessionRepositoryImpl(sessionLocalRepository, sessionRemoteRepository)

            Mockito.`when`(sessionLocalRepository.saveSessionToken(token)).thenReturn(Completable.complete())

            // When
            val testObserver = sessionRepository.refreshSessionToken().test()

            // Then
            testObserver.awaitTerminalEvent()
            testObserver.assertComplete()

            Mockito.verify(sessionLocalRepository, Mockito.only()).saveSessionToken(token)
            assert(mockAttemptSessionApi.attempt == successOn)
        }
    }
}
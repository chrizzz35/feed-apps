package com.chrizzz35.common.data.remote.response

import com.chrizzz35.common.util.SessionTestUtil
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.junit.After
import org.junit.Before
import org.junit.Test

class RefreshSessionResponseTest {

    val moshiAdapter = Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RefreshSessionResponse::class.java)

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {

    }

    @Test
    fun `RefreshSessionResponse Success, from JSON to Object`() {
        with(SessionTestUtil) {
            val responseSuccess = moshiAdapter.fromJson(refreshSessionResponseSuccessJson)

            assert(refreshSessionResponseSuccess == responseSuccess)
        }
    }

    @Test
    fun `RefreshSessionResponse Failed, from JSON to Object`() {
        with(SessionTestUtil) {
            val responseFailed = moshiAdapter.fromJson(refreshSessionResponseFailedJson)

            assert(refreshSessionResponseFailed == responseFailed)
        }
    }
}
package com.chrizzz35.common.mock

import com.chrizzz35.common.data.remote.SessionApi
import com.chrizzz35.common.data.remote.response.RefreshSessionResponse
import com.chrizzz35.common.util.SessionTestUtil
import io.reactivex.Single

class MockAttemptSessionApi (private var successOn: Int) : SessionApi {

    var attempt = 1

    override fun refreshToken(): Single<RefreshSessionResponse> {
        return Single.create<RefreshSessionResponse> {
            if(attempt < successOn) {
                attempt++

                it.onSuccess(SessionTestUtil.refreshSessionResponseFailed)
            }

            it.onSuccess(SessionTestUtil.refreshSessionResponseSuccess)
        }
    }
}
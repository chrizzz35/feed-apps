package com.chrizzz35.common.util

import com.chrizzz35.common.config.NetworkConfig
import com.chrizzz35.common.data.remote.response.RefreshSessionResponse

object SessionTestUtil {
    val messageSuccess = "Success"
    val messageFailed = "Failed"

    val token = "1234567890"


    val refreshSessionResponseSuccess = RefreshSessionResponse(NetworkConfig.API_SUCCESS, messageSuccess, token)
    val refreshSessionResponseFailed = RefreshSessionResponse(NetworkConfig.API_FAILED, messageFailed, null)

    val refreshSessionResponseSuccessJson =
            "{" +
                "\"status_code\": ${NetworkConfig.API_SUCCESS}," +
                "\"message\": \"$messageSuccess\"," +
                "\"data\": \"$token\"," +
                "\"exception\": null" +
            "}"

    val refreshSessionResponseFailedJson =
            "{" +
                "\"status_code\": ${NetworkConfig.API_FAILED}," +
                "\"message\": \"$messageFailed\"," +
                "\"data\": null," +
                "\"exception\": null" +
            "}"
}
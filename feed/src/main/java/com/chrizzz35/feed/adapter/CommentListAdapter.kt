package com.chrizzz35.feed.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.chrizzz35.feed.R
import com.chrizzz35.feed.databinding.ItemCommentBinding
import com.chrizzz35.feed.domain.entity.Comment

class CommentListAdapter(
        private val commentList: MutableList<Comment>,
        private val onCommentClick: (Comment) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, itemViewType: Int): RecyclerView.ViewHolder =
        CommentViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_comment, parent, false))

    override fun getItemCount(): Int = commentList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is CommentViewHolder) {
            viewHolder.bind(commentList[position], onCommentClick)
        }
    }

    fun updateList(commentList: List<Comment>) {
        this.commentList.clear()
        this.commentList += commentList

        notifyDataSetChanged()
    }

    class CommentViewHolder(private val binding : ItemCommentBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: Comment, onCommentClick: (Comment) -> Unit) {
            binding.comment = comment
            itemView.setOnClickListener { onCommentClick(comment) }
        }
    }
}
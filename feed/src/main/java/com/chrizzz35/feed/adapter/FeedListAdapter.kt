package com.chrizzz35.feed.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.chrizzz35.feed.R
import com.chrizzz35.feed.databinding.ItemFeedBinding
import com.chrizzz35.feed.domain.entity.Feed

class FeedListAdapter(
        private val feedList: MutableList<Feed>,
        private val onFeedClick: (Feed) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, itemViewType: Int): RecyclerView.ViewHolder =
            FeedViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_feed, parent, false))

    override fun getItemCount(): Int = feedList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is FeedViewHolder) {
            viewHolder.bind(feedList[position], onFeedClick)
        }
    }

    fun updateList(feedList: List<Feed>) {
        this.feedList.clear()
        this.feedList += feedList

        notifyDataSetChanged()
    }

    class FeedViewHolder(private val binding: ItemFeedBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(feed: Feed, onItemClick: (Feed) -> Unit) {
            binding.feed = feed
            itemView.setOnClickListener { onItemClick(feed) }
        }
    }
}
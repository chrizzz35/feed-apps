package com.chrizzz35.feed.data

import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.feed.data.local.FeedLocalRepository
import com.chrizzz35.feed.data.remote.FeedRemoteRepository
import com.chrizzz35.feed.domain.entity.Comment
import com.chrizzz35.feed.domain.entity.Feed
import com.chrizzz35.feed.domain.repository.FeedRepository
import io.reactivex.Single
import javax.inject.Inject

class FeedRepositoryImpl @Inject constructor(
        private val feedLocalRepository: FeedLocalRepository,
        private val feedRemoteRepository: FeedRemoteRepository
) : FeedRepository {

    override fun getFeedList(): Single<List<Feed>> {
        return feedRemoteRepository.getFeedList()
    }

    override fun getFeedDetail(feedId: Int): Single<Feed> {
        return feedRemoteRepository.getFeedDetail(feedId)
    }

    override fun getFeedComment(feedId: Int): Single<List<Comment>> {
        return feedRemoteRepository.getFeedComment(feedId)
    }
}
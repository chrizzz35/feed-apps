package com.chrizzz35.feed.data

import com.chrizzz35.feed.domain.entity.Comment
import com.chrizzz35.feed.domain.entity.Feed
import com.chrizzz35.feed.domain.repository.FeedRepository
import io.reactivex.Single
import javax.inject.Inject

class MockFeedRepository @Inject constructor(): FeedRepository {
    override fun getFeedList(): Single<List<Feed>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getFeedDetail(feedId: Int): Single<Feed> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getFeedComment(feedId: Int): Single<List<Comment>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
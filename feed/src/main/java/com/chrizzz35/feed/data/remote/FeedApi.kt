package com.chrizzz35.feed.data.remote

import com.chrizzz35.feed.data.remote.response.GetFeedCommentResponse
import com.chrizzz35.feed.data.remote.response.GetFeedDetailResponse
import com.chrizzz35.feed.data.remote.response.GetFeedListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface FeedApi {
    @GET("/api/feed/all")
    fun getFeedList() : Single<GetFeedListResponse>

    @GET("/api/feed/get/{feed_id}")
    fun getFeedDetail(@Path("feed_id") feedId: Int) : Single<GetFeedDetailResponse>

    @GET("/api/feed/get/comment/{feed_id}")
    fun getFeedComment(@Path("feed_id") feedId: Int) : Single<GetFeedCommentResponse>
}
package com.chrizzz35.feed.data.remote

import com.chrizzz35.feed.domain.entity.Comment
import com.chrizzz35.feed.domain.entity.Feed
import io.reactivex.Single

interface FeedRemoteRepository {
    fun getFeedList() : Single<List<Feed>>
    fun getFeedDetail(feedId: Int) : Single<Feed>
    fun getFeedComment(feedId: Int) : Single<List<Comment>>
}
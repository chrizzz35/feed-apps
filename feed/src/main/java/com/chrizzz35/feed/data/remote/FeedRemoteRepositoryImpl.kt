package com.chrizzz35.feed.data.remote

import com.chrizzz35.common.http.HttpParser
import com.chrizzz35.feed.domain.entity.Comment
import com.chrizzz35.feed.domain.entity.Feed
import io.reactivex.Single
import javax.inject.Inject

class FeedRemoteRepositoryImpl @Inject constructor(
        private val feedApi: FeedApi,
        private val httpParser: HttpParser
): FeedRemoteRepository {
    override fun getFeedList(): Single<List<Feed>> {
        return feedApi.getFeedList().map { httpParser.parseResponse(it) }
    }

    override fun getFeedDetail(feedId: Int): Single<Feed> {
        return feedApi.getFeedDetail(feedId).map { httpParser.parseResponse(it) }
    }

    override fun getFeedComment(feedId: Int): Single<List<Comment>> {
        return feedApi.getFeedComment(feedId).map { httpParser.parseResponse(it) }
    }
}
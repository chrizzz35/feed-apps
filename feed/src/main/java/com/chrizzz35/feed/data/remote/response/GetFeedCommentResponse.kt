package com.chrizzz35.feed.data.remote.response

import com.chrizzz35.common.http.HttpResponse
import com.chrizzz35.feed.domain.entity.Comment
import com.squareup.moshi.Json

data class GetFeedCommentResponse (
        @Json(name = "status_code") override val statusCode: Int?,
        @Json(name = "message") override val message: String?,
        @Json(name = "data") override val data: List<Comment>?
): HttpResponse<List<Comment>>()
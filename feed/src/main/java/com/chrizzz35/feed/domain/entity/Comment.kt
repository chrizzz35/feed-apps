package com.chrizzz35.feed.domain.entity

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Comment (
        @Json(name = "id") val id: Int?,
        @Json(name = "description") val description: String?,
        @Json(name = "created_at") val createdAt: String?,
        @Json(name = "updated_at") val updatedAt: String?,
        @Json(name = "feed_id") val feedId: Int?,
        @Json(name = "user_id") val userId: Int?,
        @Json(name = "user_name") val userName: String?,
        @Json(name = "user_email") val userEmail: String?,
        @Json(name = "user_phone") val userPhone: String?
): Parcelable
package com.chrizzz35.feed.domain.entity

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Feed (
        @Json(name = "id") val id: Int?,
        @Json(name = "image") val image: String?,
        @Json(name = "description") val description: String?,
        @Json(name = "datetime") val datetime: String?,
        @Json(name = "latitude") val latitude: Double?,
        @Json(name = "longitude") val longitude: Double?,
        @Json(name = "created_at") val createdAt: String?,
        @Json(name = "updated_at") val updatedAt: String?,
        @Json(name = "user_id") val userId: Int?,
        @Json(name = "user_name") val userName: String?,
        @Json(name = "user_email") val userEmail: String?,
        @Json(name = "user_phone") val userPhone: String?,
        @Json(name = "comments") var commentList: List<Comment>?
) : Parcelable
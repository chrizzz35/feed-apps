package com.chrizzz35.feed.domain.usecase

import com.chrizzz35.common.domain.SingleUseCase
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.feed.domain.entity.Comment
import com.chrizzz35.feed.domain.repository.FeedRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetFeedComment @Inject constructor(
        private val feedRepository: FeedRepository,
        private val sessionRepository: SessionRepository
): SingleUseCase<GetFeedComment.Param, List<Comment>> {

    class Param (val feedId: Int)

    override fun execute(param: Param): Single<List<Comment>> {
        return sessionRepository.validateSessionSingle(feedRepository.getFeedComment(param.feedId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
package com.chrizzz35.feed.domain.usecase

import com.chrizzz35.common.domain.SingleUseCase
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.feed.domain.entity.Feed
import com.chrizzz35.feed.domain.repository.FeedRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetFeedDetail @Inject constructor(
        private val feedRepository: FeedRepository,
        private val sessionRepository: SessionRepository
): SingleUseCase<GetFeedDetail.Param, Feed> {

    class Param (val feedId: Int)

    override fun execute(param: Param): Single<Feed> {
        return sessionRepository.validateSessionSingle(feedRepository.getFeedDetail(param.feedId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
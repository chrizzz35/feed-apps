package com.chrizzz35.feed.domain.usecase

import com.chrizzz35.common.domain.SingleUseCase
import com.chrizzz35.common.domain.repository.SessionRepository
import com.chrizzz35.feed.domain.entity.Feed
import com.chrizzz35.feed.domain.repository.FeedRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetFeedList @Inject constructor(
        private val feedRepository: FeedRepository,
        private val sessionRepository: SessionRepository
): SingleUseCase<GetFeedList.Param, List<Feed>> {

    class Param

    override fun execute(param: Param): Single<List<Feed>> {
        return sessionRepository.validateSessionSingle(feedRepository.getFeedList())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
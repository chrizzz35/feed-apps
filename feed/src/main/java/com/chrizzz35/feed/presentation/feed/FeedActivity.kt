package com.chrizzz35.feed.presentation.feed

import android.os.Bundle
import com.chrizzz35.feed.R
import com.chrizzz35.feed.presentation.feed.comment.FeedCommentFragment
import com.chrizzz35.feed.presentation.feed.info.FeedInfoFragment
import dagger.android.support.DaggerAppCompatActivity

const val EXTRA_FEED_ID = "extra_feed_id"

class FeedActivity : DaggerAppCompatActivity(), FeedInfoFragment.FeedInfoFragmentListener, FeedCommentFragment.FeedCommentFragmentListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)

        setupActionBar()

        if(savedInstanceState == null) {
            val feedId = intent.getIntExtra(EXTRA_FEED_ID, -1)

            val feedInfoFragment = FeedInfoFragment.newInstance(feedId)
            supportFragmentManager.beginTransaction()
                    .add(R.id.fl_content, feedInfoFragment, null)
                    .commitNow()
        }
    }

    private fun setupActionBar() {
        supportActionBar?.title = "Feed Detail"
        supportActionBar?.subtitle = "2018-05-20"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun redirectFeedComment(feedId: Int) {
        val feedCommentFragment = FeedCommentFragment.newInstance(feedId)
        supportFragmentManager.beginTransaction()
                .add(R.id.fl_content, feedCommentFragment, null)
                .addToBackStack(null)
                .commit()
    }
}

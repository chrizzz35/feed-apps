package com.chrizzz35.feed.presentation.feed

import com.chrizzz35.feed.presentation.feed.comment.FeedCommentFragment
import com.chrizzz35.feed.presentation.feed.comment.FeedCommentModule
import com.chrizzz35.feed.presentation.feed.info.FeedInfoFragment
import com.chrizzz35.feed.presentation.feed.info.FeedInfoModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FeedModule {

    @ContributesAndroidInjector(modules = [ FeedInfoModule::class ])
    abstract fun bindFeedInfoModule() : FeedInfoFragment

    @ContributesAndroidInjector(modules = [ FeedCommentModule::class ])
    abstract fun bindFeedCommentModule() : FeedCommentFragment
}
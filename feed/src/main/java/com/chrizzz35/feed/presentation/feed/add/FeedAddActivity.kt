package com.chrizzz35.feed.presentation.feed.add

import android.os.Bundle
import com.chrizzz35.feed.R
import dagger.android.support.DaggerAppCompatActivity

class FeedAddActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_add)
    }
}

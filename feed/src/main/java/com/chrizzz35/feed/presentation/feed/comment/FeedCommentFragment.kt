package com.chrizzz35.feed.presentation.feed.comment

import android.arch.lifecycle.Observer
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.feed.R
import com.chrizzz35.feed.adapter.CommentListAdapter
import com.chrizzz35.feed.databinding.FragmentFeedCommentBinding
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_feed_comment.*
import javax.inject.Inject

const val ARG_FEED_ID = "arg_feed_id"

class FeedCommentFragment : DaggerFragment() {

    private var listener: FeedCommentFragmentListener? = null
    private lateinit var binding : FragmentFeedCommentBinding

    @Inject
    lateinit var feedCommentViewModel: FeedCommentViewModel
    @Inject
    lateinit var feedCommentPresenter: FeedCommentPresenter

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var commentListAdapter: CommentListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed_comment, container, false)

        binding.setLifecycleOwner(this)
        binding.viewModel = feedCommentViewModel
        binding.presenter = feedCommentPresenter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupCommentList()

        if(savedInstanceState == null) {
            feedCommentPresenter.getFeedComment()
        }
    }

    private fun setupCommentList() {
        layoutManager = LinearLayoutManager(context)
        commentListAdapter = CommentListAdapter(arrayListOf()) {

        }

        rv_comment.layoutManager = layoutManager
        rv_comment.adapter = commentListAdapter

        feedCommentViewModel.commentList.observe(this, Observer {
            it?.let { commentList ->
                commentListAdapter.updateList(commentList)
            }
        })
    }
    
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FeedCommentFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement FeedCommentFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    interface FeedCommentFragmentListener

    companion object {
        @JvmStatic
        fun newInstance(feedId: Int) =
                FeedCommentFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_FEED_ID, feedId)
                    }
                }
    }
}

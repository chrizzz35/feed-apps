package com.chrizzz35.feed.presentation.feed.comment

import android.arch.lifecycle.ViewModelProviders
import com.chrizzz35.feed.domain.usecase.GetFeedComment
import dagger.Module
import dagger.Provides

@Module
class FeedCommentModule {
    @Provides
    fun provideFeedId(feedCommentFragment: FeedCommentFragment) : Int =
            feedCommentFragment.arguments?.getInt(ARG_FEED_ID) ?: -1

    @Provides
    fun provideFeedCommentViewModel(feedCommentFragment: FeedCommentFragment) : FeedCommentViewModel =
            ViewModelProviders.of(feedCommentFragment).get(FeedCommentViewModel::class.java)

    @Provides
    fun provideFeedCommentPresenter(feedId: Int, feedCommentViewModel: FeedCommentViewModel, getFeedComment: GetFeedComment) : FeedCommentPresenter =
            FeedCommentPresenter(feedId, feedCommentViewModel, getFeedComment)
}
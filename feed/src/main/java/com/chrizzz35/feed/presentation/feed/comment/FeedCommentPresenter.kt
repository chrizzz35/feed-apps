package com.chrizzz35.feed.presentation.feed.comment

import com.chrizzz35.feed.domain.usecase.GetFeedComment

class FeedCommentPresenter (
        private val feedId: Int,
        private val feedCommentViewModel: FeedCommentViewModel,
        private val getFeedComment: GetFeedComment
) {
    fun getFeedComment() {
        with(feedCommentViewModel) {
            if(getFeedCommentLoading.value != true) {
                compositeDisposable.add(getFeedComment.execute(GetFeedComment.Param(feedId))
                        .doOnSubscribe {
                            getFeedCommentLoading.value = true
                            commentList.value = null
                            getFeedCommentFailed.value = false
                        }
                        .doFinally {
                            getFeedCommentLoading.value = false
                        }
                        .subscribe({
                            commentList.value = it
                            getFeedCommentFailed.value = false
                        }, {
                            commentList.value = null
                            getFeedCommentFailed.value = true
                        }))
            }
        }
    }
}
package com.chrizzz35.feed.presentation.feed.comment

import android.arch.lifecycle.MutableLiveData
import com.chrizzz35.common.base.BaseViewModel
import com.chrizzz35.feed.domain.entity.Comment

class FeedCommentViewModel : BaseViewModel() {

    val commentList = MutableLiveData<List<Comment>>()
    val getFeedCommentLoading = MutableLiveData<Boolean>()
    val getFeedCommentFailed = MutableLiveData<Boolean>()
}
package com.chrizzz35.feed.presentation.feed.info

import android.arch.lifecycle.Observer
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.feed.R
import com.chrizzz35.feed.adapter.CommentListAdapter
import com.chrizzz35.feed.databinding.FragmentFeedInfoBinding
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_feed_info.*
import javax.inject.Inject

const val ARG_FEED_ID = "arg_feed_id"

class FeedInfoFragment : DaggerFragment() {

    private var listener: FeedInfoFragmentListener? = null
    private lateinit var binding: FragmentFeedInfoBinding

    @Inject
    lateinit var feedInfoViewModel: FeedInfoViewModel
    @Inject
    lateinit var feedInfoPresenter: FeedInfoPresenter

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var commentListAdapter: CommentListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed_info, container, false)

        binding.setLifecycleOwner(this)
        binding.viewModel = feedInfoViewModel
        binding.presenter = feedInfoPresenter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupCommentList()

        if(savedInstanceState == null) {
            feedInfoPresenter.getFeedDetail()
        }
    }

    private fun setupCommentList() {
        layoutManager = LinearLayoutManager(context)
        commentListAdapter = CommentListAdapter(arrayListOf()) {
            it.feedId?.let { feedId ->
                listener?.redirectFeedComment(feedId)
            }
        }

        rv_comment.layoutManager = layoutManager
        rv_comment.adapter = commentListAdapter

        feedInfoViewModel.feed.observe(this, Observer {
            it?.commentList?.let { commentList ->
                commentListAdapter.updateList(commentList)
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FeedInfoFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement FeedInfoFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface FeedInfoFragmentListener {
        fun redirectFeedComment(feedId: Int)
    }

    companion object {
        @JvmStatic
        fun newInstance(feedId: Int) =
                FeedInfoFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_FEED_ID, feedId)
                    }
                }
    }
}

package com.chrizzz35.feed.presentation.feed.info

import android.arch.lifecycle.ViewModelProviders
import com.chrizzz35.feed.domain.usecase.GetFeedDetail
import dagger.Module
import dagger.Provides

@Module
class FeedInfoModule {
    @Provides
    fun provideFeedId(feedInfoFragment: FeedInfoFragment) : Int =
            feedInfoFragment.arguments?.getInt(ARG_FEED_ID) ?: -1

    @Provides
    fun provideFeedInfoViewModel(feedInfoFragment: FeedInfoFragment) : FeedInfoViewModel =
            ViewModelProviders.of(feedInfoFragment).get(FeedInfoViewModel::class.java)

    @Provides
    fun provideFeedInfoPresenter(feedId: Int, feedInfoViewModel: FeedInfoViewModel, getFeedDetail: GetFeedDetail) : FeedInfoPresenter =
            FeedInfoPresenter(feedId, feedInfoViewModel, getFeedDetail)
}
package com.chrizzz35.feed.presentation.feed.info

import com.chrizzz35.feed.domain.usecase.GetFeedDetail

class FeedInfoPresenter (
        private val feedId: Int,
        private val feedInfoViewModel: FeedInfoViewModel,
        private val getFeedDetail: GetFeedDetail
) {
    fun getFeedDetail() {
        with(feedInfoViewModel) {
            if (getFeedDetailLoading.value != true) {
                compositeDisposable.add(getFeedDetail.execute(GetFeedDetail.Param(feedId))
                        .doOnSubscribe {
                            getFeedDetailLoading.value = true
                            feed.value = null
                            getFeedDetailFailed.value = false
                        }
                        .doFinally {
                            getFeedDetailLoading.value = false
                        }
                        .subscribe({
                            feed.value = it
                            getFeedDetailFailed.value = false
                        }, {
                            feed.value = null
                            getFeedDetailFailed.value = true
                        }))
            }
        }
    }
}
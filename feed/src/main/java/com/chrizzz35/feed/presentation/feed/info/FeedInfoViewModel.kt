package com.chrizzz35.feed.presentation.feed.info

import android.arch.lifecycle.MutableLiveData
import com.chrizzz35.common.base.BaseViewModel
import com.chrizzz35.feed.domain.entity.Feed

class FeedInfoViewModel : BaseViewModel() {

    val feed = MutableLiveData<Feed>()
    val getFeedDetailLoading = MutableLiveData<Boolean>()
    val getFeedDetailFailed = MutableLiveData<Boolean>()
}
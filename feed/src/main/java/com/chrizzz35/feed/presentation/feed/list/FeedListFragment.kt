package com.chrizzz35.feed.presentation.feed.list

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.feed.R
import com.chrizzz35.feed.adapter.FeedListAdapter
import com.chrizzz35.feed.databinding.FragmentFeedListBinding
import com.chrizzz35.feed.presentation.feed.EXTRA_FEED_ID
import com.chrizzz35.feed.presentation.feed.FeedActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_feed_list.*
import javax.inject.Inject

class FeedListFragment : DaggerFragment() {

    private var listener: FeedListFragmentListener? = null
    private lateinit var binding: FragmentFeedListBinding

    @Inject
    lateinit var feedListViewModel: FeedListViewModel
    @Inject
    lateinit var feedListPresenter: FeedListPresenter

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var feedListAdapter: FeedListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed_list, container, false)

        binding.setLifecycleOwner(this)
        binding.viewModel = feedListViewModel
        binding.presenter = feedListPresenter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupFeedList()

        if(savedInstanceState == null) {
            feedListPresenter.getFeedList()
        }
    }

    private fun setupFeedList() {
        layoutManager = LinearLayoutManager(context)
        feedListAdapter = FeedListAdapter(arrayListOf()) { feed ->
            startActivity(Intent(activity, FeedActivity::class.java).apply {
                putExtra(EXTRA_FEED_ID, feed.id)
            })
        }

        rv_feed.layoutManager = layoutManager
        rv_feed.adapter = feedListAdapter

        feedListViewModel.feedList.observe(this, Observer {
            it?.let { feed ->
                feedListAdapter.updateList(feed)
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FeedListFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement FeedListFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    
    interface FeedListFragmentListener

    companion object {
        @JvmStatic
        fun newInstance() =
                FeedListFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}

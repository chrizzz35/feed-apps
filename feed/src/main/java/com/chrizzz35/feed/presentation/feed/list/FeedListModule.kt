package com.chrizzz35.feed.presentation.feed.list

import android.arch.lifecycle.ViewModelProviders
import com.chrizzz35.feed.domain.usecase.GetFeedList
import dagger.Module
import dagger.Provides

@Module
class FeedListModule {
    @Provides
    fun provideFeedListViewModel(feedListFragment: FeedListFragment): FeedListViewModel =
            ViewModelProviders.of(feedListFragment).get(FeedListViewModel::class.java)

    @Provides
    fun provideFeedListPresenter(feedListViewModel: FeedListViewModel, getFeedList: GetFeedList): FeedListPresenter =
            FeedListPresenter(feedListViewModel, getFeedList)
}
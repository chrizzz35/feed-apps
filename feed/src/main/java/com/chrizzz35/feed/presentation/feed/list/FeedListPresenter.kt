package com.chrizzz35.feed.presentation.feed.list

import com.chrizzz35.feed.domain.usecase.GetFeedList

class FeedListPresenter (
        private val feedListViewModel: FeedListViewModel,
        private val getFeedList: GetFeedList
) {
    fun getFeedList() {
        with(feedListViewModel) {
            if(getFeedListLoading.value != true) {
                compositeDisposable.add(getFeedList.execute(GetFeedList.Param())
                        .doOnSubscribe {
                            getFeedListLoading.value = true
                            feedList.value = null
                            getFeedListFailed.value = false
                        }
                        .doFinally {
                            getFeedListLoading.value = false
                        }
                        .subscribe({
                            feedList.value = it
                            getFeedListFailed.value = false
                        }, {
                            feedList.value = null
                            getFeedListFailed.value = true
                        }))
            }
        }
    }
}
package com.chrizzz35.feed.presentation.feed.list

import android.arch.lifecycle.MutableLiveData
import com.chrizzz35.common.base.BaseViewModel
import com.chrizzz35.feed.domain.entity.Feed

class FeedListViewModel : BaseViewModel() {

    val feedList = MutableLiveData<List<Feed>>()
    val getFeedListLoading = MutableLiveData<Boolean>()
    val getFeedListFailed = MutableLiveData<Boolean>()
}
package com.chrizzz35.feed.presentation.feed.list

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.chrizzz35.common.http.HttpFailedException
import com.chrizzz35.feed.domain.entity.Feed
import com.chrizzz35.feed.domain.repository.FeedRepository
import com.chrizzz35.feed.domain.usecase.GetFeedList
import com.chrizzz35.feed.rule.RxSchedulerRule
import com.chrizzz35.feed.util.FeedTestUtil
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.*

class FeedListPresenterTest {

    @get:Rule
    val rxRule: TestRule = RxSchedulerRule()

    @get:Rule
    val liveDataRule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var feedRepository: FeedRepository

    lateinit var feedListViewModel: FeedListViewModel
    lateinit var getFeedList: GetFeedList
    lateinit var feedListPresenter: FeedListPresenter

    @Mock
    lateinit var observerFeedList: Observer<List<Feed>>
    @Mock
    lateinit var observerGetFeedListLoading: Observer<Boolean>
    @Mock
    lateinit var observerGetFeedListFailed: Observer<Boolean>

    @Captor
    lateinit var captorFeedList: ArgumentCaptor<List<Feed>>
    @Captor
    lateinit var captorGetFeedListLoading: ArgumentCaptor<Boolean>
    @Captor
    lateinit var captorGetFeedListFailed: ArgumentCaptor<Boolean>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        feedListViewModel = FeedListViewModel()
        getFeedList = GetFeedList(feedRepository)
        feedListPresenter = FeedListPresenter(feedListViewModel, getFeedList)

        feedListViewModel.feedList.observeForever(observerFeedList)
        feedListViewModel.getFeedListLoading.observeForever(observerGetFeedListLoading)
        feedListViewModel.getFeedListFailed.observeForever(observerGetFeedListFailed)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Get Feed List Success`() {
        with(FeedTestUtil) {
            // Given
            Mockito.`when`(feedRepository.getFeedList()).thenReturn(Single.just(feedList))

            // When
            feedListPresenter.getFeedList()

            // Then
            captorFeedList.run {
                Mockito.verify(observerFeedList, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == null)
                assert(endState == feedList)
            }

            captorGetFeedListLoading.run {
                Mockito.verify(observerGetFeedListLoading, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == true)
                assert(endState == false)
            }

            captorGetFeedListFailed.run {
                Mockito.verify(observerGetFeedListFailed, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == false)
                assert(endState == false)
            }
        }
    }

    @Test
    fun `Get Feed List Failed`() {
        with(FeedTestUtil) {
            // Given
            Mockito.`when`(feedRepository.getFeedList()).thenReturn(Single.error(HttpFailedException(messageFailed)))

            // When
            feedListPresenter.getFeedList()

            // Then
            captorFeedList.run {
                Mockito.verify(observerFeedList, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == null)
                assert(endState == null)
            }

            captorGetFeedListLoading.run {
                Mockito.verify(observerGetFeedListLoading, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == true)
                assert(endState == false)
            }

            captorGetFeedListFailed.run {
                Mockito.verify(observerGetFeedListFailed, Mockito.times(2)).onChanged(capture())
                val (startState, endState) = allValues

                assert(startState == false)
                assert(endState == true)
            }
        }
    }
}
package com.chrizzz35.feed.util

import com.chrizzz35.feed.domain.entity.Comment
import com.chrizzz35.feed.domain.entity.Feed

object FeedTestUtil {

    // Feed
    val id: Int = 1
    val image: String = "http:/image.com/image1"
    val description = "Description"
    val datetime = "2018-10-30 10:10:10"
    val latitude = 106.12304
    val longitude = -8.30493
    val createdAt = "2018-10-30 10:10:10"
    val updatedAt = "2018-10-30 10:10:10"
    val userId = 1
    val userName = "Christian Leonard"
    val userEmail = "christian.leonard@mail.com"
    val userPhone = "081287991931"
    var commentList = arrayListOf<Comment>()

    val feed = Feed(id, image, description, datetime, latitude, longitude, createdAt, updatedAt, userId, userName, userEmail, userPhone, commentList)

    val feedList : List<Feed>
    get() {
        val list = arrayListOf<Feed>()

        for(index in 1 until 20) {
            list += Feed(index, image, "$description $index", datetime, latitude, longitude, createdAt, updatedAt, index, userName, userEmail, userPhone, commentList)
        }

        return list
    }

    val messageSuccess = "Success"
    val messageFailed = "Failed"
}